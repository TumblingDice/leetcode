# https://leetcode-cn.com/problems/two-sum/
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        dic = {}
        for i in range(len(nums)):
            if nums[i] not in dic:
                dic[nums[i]] = [i]
            else:
                dic[nums[i]].append(i)
        for i in range(len(nums)):
            if target - nums[i] in dic:
                for j in dic[target - nums[i]]:
                    if j != i:
                        return [i, j]
        
        return [-1, -1]
        