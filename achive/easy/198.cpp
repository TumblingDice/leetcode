// https://leetcode-cn.com/problems/house-robber/
class Solution 
{
public:
    int n;
    int dp[500];
    int rob(vector<int>& nums) 
    {
        
        /***
        dp[i] = max(nums[i] + dp[i - 2], dp[i - 1])
        dp[i] updates dp[i + 1] and dp[i + 2]
        ***/
        
        n = nums.size();
        if (n ==  0)
        {
            return 0;
        }
        memset(dp, -1, sizeof(dp));
        dp[0] = nums[0];
        if (n > 1)
        {
            dp[1] = nums[1];
        }
        for (int i = 0; i < n; i++)
        {
            
            if (i + 1 < n)
            {
                dp[i + 1] = max(dp[i + 1], dp[i]);
            }
            if (i + 2 < n)
            {
                dp[i + 2] = max(dp[i + 2], dp[i] + nums[i + 2]);
            }
            
        }
        
        return dp[n - 1];
    }
};