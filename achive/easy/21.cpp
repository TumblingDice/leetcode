// https://leetcode-cn.com/problems/merge-two-sorted-lists/
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) 
    {
        auto p1 = l1, p2 = l2;
        if (l1 == nullptr && l2 == nullptr)
        {
            return nullptr;
        }

        ListNode *newList = nullptr;
        ListNode **newNode = &newList;
        while (p1 != nullptr && p2 != nullptr)
        {
            if (p1->val < p2->val)
            {
                *newNode = new ListNode(p1->val);
                p1 = p1->next;

            }
            else 
            {
                *newNode = new ListNode(p2->val);
                p2 = p2->next;
            }
            newNode = &((*newNode)->next);
        }
        
        if (p1 != nullptr)
        {
            *newNode = p1;
        }
        else 
        {
            *newNode = p2;
        }
        return newList;

        
    }
};