# https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import copy
'''
class MyNode(TreeNode):
    def __init__(self, v):
        TreeNode.__init__(self, v)
        self.father = None
        self.depth = 0
        self.size = 0
        self.top = None
        self.choice = None'''

class Solution(object):
    def __init__(self):
        self.path = []
        self.path_p = []
        self.path_q = []
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        if root == None:
            return None
        
        def search(t):
            if t == None:
                return
            
            self.path.append(t)
            if t.val == p.val:
                
                self.path_p = copy.deepcopy(self.path)
            if t.val == q.val:
                self.path_q = copy.deepcopy(self.path)

            search(t.left)
            search(t.right)

            self.path.pop()
            return
        search(root)

        #print([x.val for x in self.path_p])
        #print([x.val for x in self.path_q])
        i = min(len(self.path_p), len(self.path_q)) - 1
        
        while (i >= 0):

            if self.path_p[i].val == self.path_q[i].val:
                return self.path_p[i]
            i -= 1
        return None