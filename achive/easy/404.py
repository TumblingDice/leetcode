# https://leetcode-cn.com/problems/sum-of-left-leaves/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def sumOfLeftLeaves(self, root: TreeNode) -> int:
        
        def dfs(t: TreeNode, d: int) -> int:
            
            if (t.left == None) and (t.right == None) and (d == 0):
                return t.val
            s = 0
            if t.left != None:
                s += dfs(t.left, 0)
            
            if t.right != None:
                s += dfs(t.right, 1)
            
            return s
        
        if root == None:
            ans = 0
        else:
            ans = dfs(root, -1)

        return ans