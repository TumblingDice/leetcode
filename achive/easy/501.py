# https://leetcode-cn.com/problems/find-mode-in-binary-search-tree/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:


    def __init__(self):
        self.ans = []
        self.max = 0
    def findMode(self, root: TreeNode) -> List[int]:

        if root == None:
            return []
        else:
            self.ans = [root.val]
            self.max = 1
        def dfs(t: TreeNode, father: TreeNode, sum: int):

            if t == None:
                return

            if father != None:
                if t.val == father.val:
                    sum += 1
                    if sum > self.max:
                        self.ans = [t.val]
                        self.max = sum
                    elif sum == self.max:
                        self.ans.append(t.val)
                else:
                    sum = 1
            dfs(t.left, t, sum)
            dfs(t.right, t, sum)
            
        
        dfs(root, None, 0)
        

        return self.ans