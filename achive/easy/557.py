# https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/
class Solution(object):
    def reverseWords(self, s):
        """
        :type s: str
        :rtype: str
        """
        words = s.split(" ")
        for i in range(0, len(words)):
            words[i] = words[i][::-1]
           # print(words[i])
        
        ans = " ".join(words)

        #print(ans)
        return ans