# https://leetcode-cn.com/problems/merge-two-binary-trees/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def mergeTrees(self, root1: TreeNode, root2: TreeNode) -> TreeNode:


        def dfs(t1: TreeNode, t2: TreeNode) -> TreeNode:

            if t1 == None and t2 == None:
                return None

            elif t1 != None and t2 != None:
                t1.val += t2.val
                t1.left = dfs(t1.left, t2.left)
                t1.right = dfs(t1.right, t2.right)
                return t1
            elif t1 != None:
                return t1
            elif t2 != None:
                return t2
        
        if root1 != None and root1.val == None:
            root1 = None
        if root2 != None and root2.val == None:
            root2 = None
        root1 = dfs(root1, root2)


        return root1