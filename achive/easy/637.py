# https://leetcode-cn.com/problems/average-of-levels-in-binary-tree/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:
        if root == None:
            return []
        
        ans = []
        q = [root]

        while (len(q) != 0):
            tmp = []
            ans.append(sum([x.val for x in q]) / len(q))
            for node in q:
                if node.left != None:
                    tmp.append(node.left)
                if node.right != None:
                    tmp.append(node.right)
            q = tmp

        return ans
        