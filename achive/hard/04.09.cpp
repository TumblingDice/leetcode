// https://leetcode-cn.com/problems/bst-sequences-lcci/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */


class Solution 
{
public:
    
    vector< vector<int> > ans;
    vector<int> buf;

    deque<TreeNode *> next;
    void dfs()
    {
        if (next.size() == 0)
        {
            ans.push_back(buf);
            return;
        }

        int m = next.size();
        for (int i = 0; i < m; i++)
        {
            TreeNode *t = next.front();
            next.pop_front();
            buf.push_back(t->val);
            
            if (t->left != nullptr)
            {
                next.push_back(t->left);
            }
            if (t->right != nullptr)
            {
                next.push_back(t->right);
            }
            dfs();
            if (t->left != nullptr)
            {
                next.pop_back();
            }
            if (t->right != nullptr)
            {
                next.pop_back();
            }
            next.push_back(t);
            buf.pop_back();
        }
    }
    vector<vector<int>> BSTSequences(TreeNode* root) 
    {
        if (root == nullptr)
        {
            
            vector<int> tmp;
            vector<vector<int>> ret;
            ret.push_back(tmp);
            return ret;
        }
        next.push_back(root);
        dfs();
        return ans;
    }
};