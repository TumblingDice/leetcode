// https://leetcode-cn.com/problems/pile-box-lcci/

// 我发现了, 这个关系好像是个lattice啊
// 那这个题其实求的就是一个lattice的某种特殊定义下的高度
// 方法可以是求一个有向图两点的最大权路径.


class Solution {
public:
    int n;
    struct Edge;
    struct Node
    {
        vector<Edge> neighbor;
        int dist;
        int id;
        Node()
        {

            dist = -1;
        }
        friend bool operator  < (const Node &x, const Node &y) 
        {
            return x.dist < y.dist;
        }
    } node[3005];
    struct Edge
    {
        int weight;
        Node *end;
        Edge(Node *e, int w)
        {
            weight = w;
            end = e;
        }
    };
    // dijkstra won't work
    int longestPath(Node *s, Node *t)
    {
        if (s == t)
        {
            return 0;
        }
        if (s->dist != -1)
        {
            return s->dist;
        }

        int res = 0;
        for (Edge edge: s->neighbor)
        {
            Node *v = edge->end;
            res = max(res, edge->weight + longestPath(v, t));
        }
        
        s->dist = res;
        return res;
    }


    int pileBox(vector<vector<int>>& box) 
    {
        n = box.size();
        
        
        // build graph
        Node *start = &node[n];
        Node *terminal = &node[n + 1];
        start->id = n;
        terminal->id = n + 1;
        for (int i = 0; i < n; i++)
        {
            start->neighbor.push_back(Edge (&node[i], box[i][2]));
            node[i].neighbor.push_back(Edge(terminal, 0));
        }
        start->neighbor.push_back(Edge(terminal, 0));
        // 这里也许能优化一下?
        for (int i = 0; i < n; i++)
        {
            node[i].id = i;
            for (int j = i + 1; j < n; j++)
            {
                if (box[i][0] < box[j][0] && box[i][1] < box[j][1] && box[i][2] < box[j][2])
                {
                    node[i].neighbor.push_back(Edge(&node[j], box[j][2]));
                }
                else if (box[i][0] > box[j][0] && box[i][1] > box[j][1] && box[i][2] > box[j][2])
                {
                    node[j].neighbor.push_back(Edge(&node[i], box[i][2]));
                }
            }
        }
        
        terminal->dist = 0;
        int ans = longestPath(start, terminal);


        return ans;
    }
};