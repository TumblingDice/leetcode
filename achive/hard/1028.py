# https://leetcode-cn.com/problems/recover-a-tree-from-preorder-traversal/

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def getNextNode(self, s: str) -> (int, int):
        i = 0
        while (i < len(s)):
            depth = 0
            
            while (i < len(s) and s[i] == "-"):
                depth += 1
                i += 1
            
            l = i
            while (i < len(s) and s[i] != "-"):
                i += 1
            r = i

            val = int(s[l: r])
            yield((depth, val))

    def recoverFromPreorder(self, S: str) -> TreeNode:
        stack = []
        root = None
        for (depth, val) in self.getNextNode(S):
            newNode = TreeNode(val, None, None)
            if (len(stack) == 0):
                root = newNode
                stack.append((newNode, 0))
            else:
                
                while (len(stack) > 0):
                    (curNode, dep) = stack[-1]
                    if (dep >= depth):
                        stack.pop(-1)
                    else:
                        if (curNode.left == None):
                            curNode.left = newNode
                        else:
                            curNode.right = newNode
                        
                        break
            stack.append((newNode, depth))
        
        return root