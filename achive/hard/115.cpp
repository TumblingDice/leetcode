// https://leetcode-cn.com/problems/distinct-subsequences/
class Solution {
public:


    int **dp;
    int n, m;
    string pattern, str;
    int solve(int a, int b)
    {
        if (b == m)
        {
            return 1;
        }
        if (a == n)
        {
            return 0;
        }
        if (dp[a][b] != -1)
        {
            return dp[a][b];
        }
        
        int ret = 0;
        if (pattern[b] == str[a])
        {
            ret += solve(a + 1, b + 1);
        }
        ret += solve(a + 1, b);

        return dp[a][b] = ret;
    }


    int numDistinct(string s, string t) 
    {
        n = s.length();
        m = t.length();
        pattern = t;
        str = s;
        dp = new int* [n + 1];
        for (int i = 0; i < n; i++)
        {
            dp[i] = new int [m + 1];
            for (int j = 0; j < m; j++)
            {
                dp[i][j] = -1;
            }
        }

        /*for (int i = 0; i < m; i++)
        {
            dp[n][i] = 0;
        }*/

        return solve(0, 0);
    }
};