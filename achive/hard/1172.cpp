// https://leetcode-cn.com/problems/dinner-plate-stacks/


class DinnerPlates {
public:

    struct Plate
    {
        stack<int> *stk;
        int idx;
        bool operator < (const Plate &other) const 
        {
            return this->idx < other.idx;
        }
    };
    int capacity;
    map<int, stack<int> *> nonFull;
    map<int, stack<int> *> nonEmpty;


    vector<stack<int> *> refered;


    DinnerPlates(int capacity) 
    {
        this->capacity = capacity;
    }
    
    void push(int val) 
    {
        if (nonFull.size() == 0)
        {
            stack<int> *newStack = new stack<int>;

            int idx = refered.size();
            newStack->push(val);
            refered.push_back(newStack);
            nonEmpty.insert(pair<int, stack<int> *>(idx, newStack));
            if (capacity != 1)
            {
                nonFull.insert(pair<int, stack<int> *>(idx, newStack));
            }
        }
        else 
        {
            int idx = nonFull.begin()->first;
            stack<int> *targetStack = nonFull.begin()->second;
            targetStack->push(val);
            if (targetStack->size() == capacity)
            {
                nonFull.erase(nonFull.begin());
            }
            if (targetStack->size() == 1)
            {
                nonEmpty.insert(pair<int, stack<int> *>(idx, targetStack));
            }
        }
    }
    
    int pop() 
    {
        if (nonEmpty.size() == 0)
        {
            return -1;
        }
        else 
        {
            int idx = nonEmpty.rbegin()->first;
            stack<int> *targetStack = nonEmpty.rbegin()->second;

            int ret = targetStack->top();
            targetStack->pop();
            if (targetStack->size() == 0)
            {
                nonEmpty.erase(idx);
            }
            if (targetStack->size() == capacity - 1)
            {
                nonFull.insert(pair<int, stack<int> *>(idx, targetStack));
            }
            return ret;
        }
    }
    
    int popAtStack(int index) 
    {
        if (index >= refered.size())
        {
            return -1;
        }
        else 
        {
            if (refered[index]->size() == 0)
            {
                return -1;
            }
            else 
            {
                int ret = refered[index]->top();
                refered[index]->pop();
                if (refered[index]->size() == 0)
                {
                    nonEmpty.erase(index);
                }
                if (refered[index]->size() == capacity - 1)
                {
                    nonFull.insert(pair<int, stack<int> *>(index, refered[index]));
                }
                return ret;
            }
        }
    }
};

/**
 * Your DinnerPlates object will be instantiated and called as such:
 * DinnerPlates* obj = new DinnerPlates(capacity);
 * obj->push(val);
 * int param_2 = obj->pop();
 * int param_3 = obj->popAtStack(index);
 */
