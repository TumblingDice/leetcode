// https://leetcode-cn.com/problems/critical-connections-in-a-network/

class Solution 
{
public:
    static const int MAX_N = 100005;
    int counter = 0;
    vector<vector<int>> cutEdge;
    struct Node 
    {
        
        vector<int> neighbor;
        int stamp;
        int low;
        Node()
        {
            stamp = -1;
            low = -1;
        }
    };
    Node node[MAX_N];



    void dfs(int u, int parent)
    {
        if (node[u].stamp == -1)
        {
            node[u].stamp = counter;
            node[u].low = counter;
            counter++;
            
            for (auto v: node[u].neighbor)
            {
                if (v == parent)
                {
                    continue;
                }
                dfs(v, u);
                if (node[v].low > node[u].stamp)
                {
                    vector<int> tmp;
                    tmp.push_back(u);
                    tmp.push_back(v);
                    cutEdge.push_back(tmp);
                }
                node[u].low = min(node[u].low, node[v].low);
            }
        }
        else 
        {

        }
    }
    vector<vector<int>> criticalConnections(int n, vector<vector<int>>& connections) 
    {
        for (auto edge: connections)
        {
            node[edge[0]].neighbor.push_back(edge[1]);
            node[edge[1]].neighbor.push_back(edge[0]);
        }
        dfs(0, -1);
        return cutEdge;
    }
};