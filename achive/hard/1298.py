# https://leetcode-cn.com/problems/maximum-candies-you-can-get-from-boxes/
class Solution:
    def maxCandies(self, status: List[int], candies: List[int], keys: List[List[int]], containedBoxes: List[List[int]], initialBoxes: List[int]) -> int:
        
        
        #unlocked = set([x for x in range(0, len(status)) if x == 1])

        unlocked = set()
        for i in range(0, len(status)):
            if status[i] == 1:
                unlocked.add(i)
        acquired = set(initialBoxes)
        
        visited = set()


        q = [x for x in (unlocked & acquired)]
        visited.update(q)
        ans = 0
       
        while (len(q) != 0):
            
            t = q.pop(0)
            ans += candies[t]
            
            unlocked.update(keys[t])
            acquired.update(containedBoxes[t])

            m = (unlocked & acquired) - visited
            q.extend(m)
            visited.update(m)

            
        return ans