// https://leetcode-cn.com/problems/minimum-insertion-steps-to-make-a-string-palindrome/submissions/
class Solution {
public:
    int res[555][555];
    string s;
    int solve(int l, int r)
    {
        //printf("(%d, %d)\n", l, r);
        if (l >= (r - 1))
        {
            return 0;
        }

        if (res[l][r] != -1)
        {
            return res[l][r];
        }
        int ret;
        if (s[l] == s[r - 1])
        {
            
            ret = solve(l + 1, r - 1);
        }
        else
        {
            ret = min(solve(l + 1, r), solve(l, r - 1)) + 1;
        }
        res[l][r] = ret;
        return ret;
    }

    int minInsertions(string s) 
    {
        memset(res, -1, sizeof(res));
        this->s = s;

        return solve(0, s.length());
    }
};