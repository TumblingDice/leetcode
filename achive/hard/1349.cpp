// https://leetcode-cn.com/problems/maximum-students-taking-exam/

class Solution
{
public:

    int m, n;

    int graph[100][100];
    int match[100];
    int visit[100];
    int idx(int i, int j)
    {
        return (i * m + j);
    }
    int augment(int u)
    {
        
        for (int v = 0; v < n * m; v++)
        {
            if (!graph[u][v])
            {
                continue;
            }
            if (visit[v])
            {
                continue;
            }
            visit[v] = 1;
            //printf("%d with %d\n", v, match[v]);
            if (match[v] == -1)
            {
                match[u] = v;
                match[v] = u;
                return 1;
            }
            else if (augment(match[v]))
            {
                match[u] = v;
                match[v] = u;
                return 1;
            }
            
        }
        return 0;
    }


    // 实际上是个二分图求最大独立集
    // 正好复习一下图论:
    // 对二分图而言, 最大匹配的边数(最大边独立集) = 最小覆盖的点数(最小点覆盖集)
    // 而对于一般的图而言, 最大点独立集和最小点覆盖集是互补的.
    int maxStudents(vector<vector<char>>& seats)
    {
        m = seats.size();
        n = seats[0].size();
        memset(graph, 0, 4 * 100 * 100);
        memset(match, -1, 4 * 100);
        int count = 0;
        // n layers
        //printf("n: %d, m: %d\n", n, m);
        // construct the graph
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = 0; j < m; j++)
            {
                //printf("what the funk m = %d  n = %d\n", j, i);
                if (seats[j][i] == '.')
                {
                    // a valid node.
                    count ++;
                    int u = idx(i, j);
                    if (seats[j][i + 1] == '.')
                    {
                        int v = idx(i + 1, j);
                        graph[u][v] = 1;
                        graph[v][u] = 1;
                    }
                    
                    if (j > 0 && seats[j - 1][i + 1] == '.')
                    {
                        int v = idx(i + 1, j - 1);
                        graph[u][v] = 1;
                        graph[v][u] = 1;
                    }
                    if (j < m - 1 && seats[j + 1][i + 1] == '.')
                    {
                        int v = idx(i + 1, j + 1);
                        graph[u][v] = 1;
                        graph[v][u] = 1;
                    }
                }
            }
        }
        for (int j = 0; j < m; j++)
        {
            if (seats[j][n - 1] == '.')
            {
                count++;
            }
        }

        /*for (int i = 0; i < 100; i++)
        {
            for (int j = 0; j < 100; j++)
            {
                if (graph[i][j])
                {
                    printf("%d %d\n", i, j);
                }
            }
        }*/

        int maxMatch = 0;
        for (int i = 0; i < n; i += 2)
        {
            for (int j = 0; j < m; j++)
            {
                if (seats[j][i] != '.')
                {
                    continue;
                }
                memset(visit, 0, 4 * 100);
                int u = idx(i, j);
                if (augment(u))
                {
                    //printf("%d matched with %d\n", u, match[u]);
                    maxMatch++;
                }
            }
        }
        //printf("max match: %d\n", maxMatch);
        return count - maxMatch;
    }
};