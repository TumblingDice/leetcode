// https://leetcode-cn.com/problems/construct-target-array-with-multiple-sums/
class Solution 
{
public:
    bool isPossible(vector<int>& target) 
    {
        priority_queue<int> q;
        long long sum = 0;
        for (auto num: target)
        {
            sum += num;
            q.push(num);
        }
        while (true)
        {
            // 好像全开long long确实很慢
            long long maxNum = q.top();
            q.pop();
            if (maxNum == 1)
            {
                return true;
            }
            //int prior = sum - maxNum;
            if (sum - maxNum < 1)
            {
                // in this case (prior = sum - maxNum) is impossible to be made up.
                return false;
            }
            // keep decreasing till (maxNum) is no longer maxium, i.e. less than the current heap top.
            // and this (k) is simply for acceleration, 加速左转
            long long k = (maxNum - q.top() - 1) / (sum - maxNum) + 1;
            
            long long prior = maxNum - (sum - maxNum) * k;
            if (prior < 1)
            {
                return false;
            }
            q.push(prior);
            sum -= (sum - maxNum) * k;
        }

        return true;
    }
};