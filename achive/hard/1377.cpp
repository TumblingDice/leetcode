// https://leetcode-cn.com/problems/frog-position-after-t-seconds/
class Solution {
public:
    vector<int> neighbor[101];
    int visit[101];
    struct State
    {
        int pos;
        double prob;
        int time;
        State(int pos, double prob, int time)
        {
            this->pos = pos;
            this->prob = prob;
            this->time = time;
        }
    };
    // 本来以为是树状dp
    // 发现bfs就行
    //又想了一下好像dfs更好写
    double frogPosition(int n, vector<vector<int>>& edges, int t, int target) 
    {
        
        for (int i = 0; i < edges.size(); i++)
        {
            int& u = edges[i][0];
            int& v = edges[i][1];
            neighbor[u].push_back(v);
            neighbor[v].push_back(u);
        }

        queue<State> q;
        q.push(State(1, 1.0, 0));
        
        while (!q.empty())
        {
            
            State s = q.front();
            q.pop();
            visit[s.pos] = 1;
            //printf("%d %f %d\n", s.pos, s.prob, s.time);
            if (s.pos == target)
            {
                if (s.time == t)
                {
                    return s.prob;
                }
                else if (s.time > t)
                {
                    return 0;
                }
                else
                {
                    for (auto v: neighbor[s.pos])
                    {
                        if (!visit[v])
                        {
                            return 0;
                        }
                    }
                    return s.prob;
                }
            }
            if (s.time > t)
            {
                return 0;
            }
            int count = 0;
            for (auto v: neighbor[s.pos])
            {
                if (!visit[v])
                {
                    count++;
                }
            }
            for (auto v: neighbor[s.pos])
            {
                if (!visit[v])
                {
                    q.push(State(v, s.prob / count, s.time + 1));
                }
            }
        }

        return 666;
    }
};