// https://leetcode-cn.com/problems/maximum-performance-of-a-team/
class Solution 
{
public:
    struct Eng
    {
        int speed;
        int efficiency;
        static bool myCmp(const Eng& x, const Eng& y)
        {
            return x.efficiency > y.efficiency;
        }
        friend bool operator < (const Eng& x, const Eng& y) 
        {
            return x.speed > y.speed;
        }
    };
    vector<Eng> eng;
    int maxPerformance(int n, vector<int>& speed, vector<int>& efficiency, int k) 
    {
        #define MOD 1000000007
        for (int i = 0; i < n; i++)
        {
            Eng tmp;
            tmp.speed = speed[i];
            tmp.efficiency = efficiency[i];
            eng.push_back(tmp);
        }
        sort(eng.begin(), eng.end(), Eng::myCmp);
        
        for (int i = 0; i < n; i++)
        {
            //printf("%d, %d\n", eng[i].speed, eng[i].efficiency);
        }
        
        priority_queue<Eng> q;
        long long sum = 0;
        long long ans = 0;
        for (int i = 0; i < k; i++)
        {
            sum += eng[i].speed;
            q.push(eng[i]);
            ans = max(ans, sum * eng[i].efficiency);
        }
        


        
        for (int i = k; i < n; i++)
        {
            if (q.top().speed < eng[i].speed)
            {
                sum -= q.top().speed;
                q.pop();
                q.push(eng[i]);
                sum += eng[i].speed;
                ans = max(ans, sum * eng[i].efficiency);
            }
        }

        return ans % MOD;

    }
};