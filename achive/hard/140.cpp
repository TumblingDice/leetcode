// https://leetcode-cn.com/problems/word-break-ii/
// 搞个trie, 做dfs, 每层都遍历一下trie. 
// 居然有一个case来攻击到指数级别. 用了个alphabet来快速特判
// 太恶心了
// 把trie进化到ac自动机上做dp能解决吗?
// 其实好像在dfs上加个记忆化就能解决?
// 但一道leetcode题会有这么麻烦吗?
class Solution 
{
public:
// trie部分.
// 没单独写一个类是因为太麻烦了


    unordered_set<char> alphabet;
    inline int idx(char c)
    {
        return int(c) - int('a');
    }
    class Node
    {
    public:
        Node()
        {
            terminal = -1;
            for (int i = 0; i < 30; i++)
            {
                child[i] = nullptr;
            }
        }
        Node *child[30];
        
        int terminal; // -1: not a terminal; positive: index of dic
    };
    Node *root;
    vector<string> *dic;
    void insert(int k)      // insert k-th string in dic
    {
        Node *t = root;
        string s = (*dic)[k];
        //cout << "insert " << s << endl;
        for (int i = 0; i < s.length(); i++)
        {
            int m = idx(s.at(i));
            if (t->child[m] == nullptr)
            {
                t->child[m] = new Node ();
            }
            t = t->child[m];
        }
        t->terminal = k;
    }
    string str;
    vector<vector<int> > ans;
    vector<int> buf;
    void dfs(int pos)
    {
        printf("new branch at %d\n", pos);
        if (pos == str.length())
        {
            
            ans.push_back(buf);
        }
        Node *t = root;
        while (pos != str.length())
        {
            Node *c = t->child[idx(str.at(pos))];
            //printf("pos: %d\n", pos);
            if (c == nullptr)
            {
                //printf("dead end, return\n");
                return;
            }
            else if (c->terminal >= 0)
            {
                // meet a word in trie
                //printf("meet a word: %d\n", c->terminal);
                buf.push_back(c->terminal);
                dfs(pos + 1);
                buf.pop_back();
            }
            else 
            {
                //printf("just matched, nothing special\n");
            }
            t = c;
            pos++;
        }
    }
    
    vector<int> pbuf;
    void print(Node *t)
    {
        if (t->terminal >= 0)
        {
            cout << "meet a word:" << endl;
            for (auto p: pbuf)
            {
                cout << p << " ";
            }
            cout << endl << (*dic)[t->terminal] << endl;
        }

        for (int i = 0; i < 30; i++)
        {
            if (t->child[i] != nullptr)
            {
                pbuf.push_back(i);
                print(t->child[i]);
                pbuf.pop_back();
            }
        }

    }
    
    vector<string> wordBreak(string s, vector<string>& wordDict) 
    {
        
        root = new Node ();
        dic = &wordDict;
        str = s;
        for (int i = 0; i < wordDict.size(); i++)
        {
            for (int j = 0; j < wordDict[i].length(); j++)
            {
                alphabet.insert(wordDict[i].at(j));
            }
        }
        for (int i = 0; i < s.length(); i++)
        {
            if (alphabet.find(s.at(i)) == alphabet.end())
            {
                vector<string> tmp;
                return tmp;
            }
        }
        for (int i = 0; i < wordDict.size(); i++)
        {
            insert(i);
        }
        //cout << ("insert done\n");


        //print(root);
        dfs(0);

        vector<string> ret;
        //printf("ans size: %d\n", ans.size());
        for (auto sentence: ans)
        {
            
            string tmp;
            for (int i = 0; i < sentence.size() - 1; i++)
            {
                tmp += wordDict[sentence[i]] + " ";
            }
            tmp += wordDict[sentence[sentence.size() - 1]];
            ret.push_back(tmp);
        }

        return ret;
    }
};