
// https://leetcode-cn.com/problems/max-dot-product-of-two-subsequences/
class Solution {
public:
    int dp[555][555];
    vector<int> a, b;
    int solve(int i, int j)
    {
        if (dp[i][j] != -1)
        {
            return dp[i][j];
        }

        dp[i][j] = solve(i + 1, j + 1) + a[i] * b[j];
        // 左右脚一起迈等于一个个迈
        dp[i][j] = max(dp[i][j], solve(i + 1, j));
        dp[i][j] = max(dp[i][j], solve(i, j + 1));
        return dp[i][j];
    }
    int maxDotProduct(vector<int>& nums1, vector<int>& nums2) 
    {
        // dp[i][j]: 第一个序列i项以后, 第二个序列j项以后的最优解

        a = nums1;
        b = nums2;

        
        memset(dp, -1, 4 * 555 * 555);
        int x = (a[0] < 0) ? (-a[0]) : (a[0]);
        int y = (b[0] < 0) ? (-b[0]) : (b[0]);
        for (int i = 0; i < b.size(); i++)
        {
            dp[a.size()][i] = 0;
            y = min(y, (b[i] < 0) ? (-b[i]) : (b[i]));
        }
        for (int i = 0; i < a.size(); i++)
        {
            dp[i][b.size()] = 0;
            x = min(x, (a[i] < 0) ? (-a[i]) : (a[i]));
        }
        dp[a.size()][b.size()] = 0;
        int ans = solve(0, 0);
        //printf("%d, %d %d\n", ans, x, y);
        if (ans != 0)
        {
            return ans;
        }
        else 
        {
            // hack for negative answer
            return -x * y;
        }
        
    }
};