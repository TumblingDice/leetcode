// https://leetcode-cn.com/problems/cherry-pickup-ii/

class Solution 
{
public:
    int dp[100][100][100];     // dp[level][p1][p2]  : 从第level层开始, 第一个人在p1, 第二个人在p2, 此后的最优解
    
    int transfer[3] = {-1, 0, 1};
    
    vector<vector<int>> *g;
    int solve(int level, int p1, int p2)
    {
        if (dp[level][p1][p2] != -1)
        {
            return dp[level][p1][p2];
        }
        int res = 0;
        for (int i = 0; i < 3; i++)
        {
            int nextP1 = p1 + transfer[i];
            for (int j = 0; j < 3; j++)
            {
                int nextP2 = p2 + transfer[j];
                if (nextP1 >= 0 && nextP1 < (*g)[0].size() && nextP2 >= 0 && nextP2 < (*g)[0].size())
                {
                    res = max(res, solve(level + 1, nextP1, nextP2));
                }
                
            }
        }
        if (p1 == p2)
        {
            res += (*g)[level][p1];
        }
        else 
        {
            res += (*g)[level][p1] + (*g)[level][p2];
        }
      
        dp[level][p1][p2] = res;
        return res;
        
    }
    
    int cherryPickup(vector<vector<int>>& grid) 
    {
        g = &grid;
        memset(dp, -1, 100 * 100 * 100 * 4);
        for (int i = 0; i < grid[0].size(); i++)
        {
            for (int j = 0; j < grid[0].size(); j++)
            {
                //printf("%d %d %d\n", grid.size(), i, j);
                dp[grid.size()][i][j] = 0;
            }
        }


        int ans = solve(0, 0, grid[0].size() - 1);
        return ans;
    }
};