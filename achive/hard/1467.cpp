// https://leetcode-cn.com/problems/probability-of-a-two-boxes-having-the-same-number-of-distinct-balls/
class Solution 
{
public:
    int diff;
    int nBall;
    int nColor;
    vector<int> ball;
    int comb[51][51];
    int power[8];

    double search(int depth, int diff, int m)
    {
        double ret = 0;
        if (depth == nColor)
        {
            if (diff == 0 && m == nBall / 2)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        if (diff > nColor - depth + 1)
        {
            return 0;
        }

        if (m > nBall / 2)
        {
            return 0;
        }
        
        ret += (1.0 / power[ball[depth]]) * search(depth + 1, diff - 1, m);
        // for (int i = 1; i < min(ball[i], nBall / 2 - m + 1); i++)
        for (int i = 1; i < ball[depth]; i++)
        {
            ret += (comb[i][ball[depth]] * 1.0 / power[ball[depth]]) * search(depth + 1, diff, m + i);
        }
        ret += (1.0 / power[ball[depth]]) * search(depth + 1, diff + 1, m + ball[depth]);


        return ret;
    }


    double getProbability(vector<int>& balls) 
    {
        nBall = 0;
        for (auto i: balls)
        {
            nBall += i;
        }
        nColor = balls.size();
        ball = balls;

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                comb[i][j] = 0;
            }
        }
        /*for (int i = 0; i < 7; i++)
        {
            comb[i][0] = 0;
        }*/
        comb[0][0] = 1;
        
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                comb[i][j + 1] += comb[i][j];
                comb[i + 1][j + 1] += comb[i][j];
            }
        }
        for (int i = 0, b = 1; i < 7; i++)
        {
            power[i] = b;
            b = b * 2;
        }
        double ans = 0;
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                //printf("comb %d, %d: %d\n", i, j, comb[i][j]);
            }
        }
        
        


        long long p = 1;
        for (int i = 0; i < nBall; i++)
        {
            p = p * 2;
        }
        double g = 1, h = 1;
        for (int i = nBall / 2 + 1; i <= nBall; i++)
        {
            g = g * i;
        }
        for (int i = 1; i <= nBall / 2; i++)
        {
            h = h * i;
        }
        double a = search(0, 0, 0);
        double b = (g / h / p);
        ans = a / b;
        printf("%f %f %f\n", a, b, ans);
        return ans;
    }
};