# https://leetcode-cn.com/problems/max-points-on-a-line/

# 原来不用dp, 枚举然后哈希就行

class Solution(object):
    
    def gcd(self, x, y):
        return x if (y == 0) else self.gcd(y, x % y)


    def maxPoints(self, points):
        """
        :type points: List[List[int]]
        :rtype: int
        """
        n = len(points)
        ret = 0
        for i in range(0, n):
            s = 0
            dic = {}
            ans = 1
            for j in range(i + 1, n):
                (dx, dy) = (points[i][0] - points[j][0], points[i][1] - points[j][1])
                
                (keyx, keyy) = (dx, dy)
                if (dx == 0 and dy == 0):
                    s += 1
                    continue
                elif (dx == 0):
                    (keyx, keyy) = (0, 1)
                elif (dy == 0):
                    (keyx, keyy) = (1, 0)
                else:
                    
                    g = self.gcd(abs(dx), abs(dy))
                    (keyx, keyy) = (dx / g, dy / g)
                    if (keyx * keyy < 0):
                        (keyx, keyy) = (abs(keyx), -abs(keyy))
                    else:
                        (keyx, keyy) = (abs(keyx), abs(keyy))
                if ((keyx, keyy) not in dic):
                    dic[(keyx, keyy)] = 2
                else:
                    dic[(keyx, keyy)] += 1
                ans = max(ans, dic[(keyx, keyy)])
            ret = max(ans + s, ret)
            

        return ret