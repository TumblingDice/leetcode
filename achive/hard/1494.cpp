# https://leetcode-cn.com/problems/parallel-courses-ii/
class Solution {
public:
/*
    struct Node
    {
        vector<int> child;
        //vector<int> father;
        vector<int> father = 0;
        bool done;
        bool operator < (const Node& x, const Node& y)
        {
            return x.nFather < y.nFather;
        }
    } node[15];*/
    int n, k;
    int courseDepMask[15];              // dependencies mask for each course
    int setDepMask[(1 << 15)];       // dependencies mask for a set of course to take.
    int setValid[1 << 15];              // is this set of course to take valid? i.e. none of them are dependended.
    int sol[1 << 15];
    #define MAX_N_STATE (1 << n)
    #define ACCEPT_STATE (MAX_N_STATE - 1)
    #define COURSE_MASK(course) ((1 << (course)))
    #define DEP_MET(state, dep) ((state & dep) == dep)
/*

    int update(int state, int last, int numToTake)
    {
        if (numToTake == 0)
        {
            return dfs(state);
        }
        int ret = n + 1;
        for (int i = lastTake + 1; i < n; i++)
        {
            if (((((1 << i) & state) == 0)  & (((preMask[i] & state) == preMask[i]))))
            {
                // can take this course.
                
                ret = min(update(state | (1 << i), i, numToTake - 1), update(state, i, numToTake));
                break;
                
            }

        }

        if (state )
    }
    

*/


    int dfs(int state)
    {
        
        if (state == ACCEPT_STATE)
        {
            return 0;
        }
        if (sol[state] != -1)
        {
            return sol[state];
        }
        int ret = n + 1;

        //printf("state: %d\n", state);
        /*
        update(state);
        for (int i = 0; i < n; i++)
        {
            if (!((((1 << i) & state) == 0)  & (((preMask[i] & state) == preMask[i]))))
            {
                continue;
            }
        }
        */
        int maxCourseToTake = 0;
        for (int course = 0; course < n; course++)
        {
            //printf("course# %d\tmask: %d\tdep: %d\n", course, COURSE_MASK(course), courseDepMask[course]);
            if (((COURSE_MASK(course) & state) == 0) && (DEP_MET(state, courseDepMask[course])))    // pre-calculate the exact number of courses that we can actually take in this turn.
            {
                maxCourseToTake += 1;
            }
        }
        maxCourseToTake = min(maxCourseToTake, k);
        //printf("mtk: %d\n", maxCourseToTake);
        for (int setToTake = 0; setToTake < MAX_N_STATE; setToTake++)         // setToTake:  choose k courses, but some of them may be already in this state set, just for enumeration.
        {
            int setDoTake = ((~state) & (setToTake));           // {courses that actually newly added}
            //printf("sdt: %d\n", setDoTake);
            if (__builtin_popcount(setDoTake) == maxCourseToTake)          // match since we pre-calculated.
            {
                
               // printf("kd: %d\n", setToTake);
                if (DEP_MET(state, setDepMask[setDoTake]))      // I can take all of these, i.e. dependencies are all met.
                {
                            //  != empty
                    {
                        if (setValid[setDoTake])       // no overtaking
                        {
                            //
                            ret = min(ret, dfs(state | setDoTake) + 1);
                        }
                    }
                    
                }
            }
        }

       // printf("%d returns: %d\n", state, ret);
        return (sol[state] = ret);
    }

    int minNumberOfSemesters(int n, vector<vector<int>>& dependencies, int k) 
    {
        memset(courseDepMask, 0, sizeof(courseDepMask));
        memset(setValid, 0, sizeof(setValid));
        memset(setDepMask, 0, sizeof(setDepMask));
        memset(sol, -1, sizeof(sol));
        this->n = n;
        this->k = min(k, n);
       // printf(">>>>>%d\n", MAX_N_STATE);
        for (auto edge: dependencies)
        {
            courseDepMask[edge[1] - 1] |= COURSE_MASK(edge[0] - 1);
        }

        for (int state = 0; state < MAX_N_STATE; state++)
        {
            for (int course = 0; course < n; course++)
            {
                if ((COURSE_MASK(course) & state) != 0)
                {
                    setDepMask[state] |= courseDepMask[course];
                }
            }
            setValid[state] = ((state & (setDepMask[state])) == 0);
        }

        int ans = dfs(0);
        return ans;
    }
};