// https://leetcode-cn.com/problems/get-the-maximum-score/

class Solution 
{
public:
    int maxSum(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<long long> preSum1, preSum2;
        vector<pair<int, int>> interval;
        int i = 0;
        int j = 0;
        preSum1.push_back(0);
        preSum2.push_back(0);
        while (i < nums1.size() && j < nums2.size())
        {
            
            if (nums1[i] < nums2[j])
            {
                preSum1.push_back(nums1[i] + preSum1.back());
                i++;
            }
            else if (nums1[i] > nums2[j])
            {
                preSum2.push_back(nums2[j] + preSum2.back());
                j++;
            }
            else 
            {
                interval.push_back(pair<int, int> (i, j));
                preSum1.push_back(nums1[i] + preSum1.back());
                preSum2.push_back(nums2[j] + preSum2.back());
                i++;
                j++;
            }
            
        }
        while (i < nums1.size())
        {
            preSum1.push_back(nums1[i] + preSum1.back());
            i++;
        }
        while (j < nums2.size())
        {
            preSum2.push_back(nums2[j] + preSum2.back());
            j++;
        }
        #define MOD 1000000007
        long long ans = 0;
        int last1 = 0;
        int last2 = 0;
        for (int i = 0; i < interval.size(); i++)
        {
            //printf("(%d, %d)\n", interval[i].first, interval[i].second);
            long long s1 = preSum1[interval[i].first] - preSum1[last1];
            long long s2 = preSum2[interval[i].second] - preSum2[last2];
            //printf("s: %d, %d\n", s1, s2);
            ans = ((long long)ans + max(s1, s2) % MOD) % MOD;
            last1 = interval[i].first;
            last2 = interval[i].second;
        }
        ans = ((long long)ans + max((preSum1[nums1.size()] - preSum1[last1]), (preSum2[nums2.size()] - preSum2[last2])) % MOD ) % MOD;
        return (int) ans;
    }
};