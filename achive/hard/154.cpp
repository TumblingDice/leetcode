// https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array-ii/

// 算法课的作业题
class Solution 
{
public:

    int n;

    int findMin(vector<int>& nums) 
    {
        n = nums.size();
        int l = 0;
        int r = n - 1;
        while (l < r)
        {
            int mid = (l + r) / 2;
            int pivot = nums[mid];
            if (pivot < nums[r])
            {
                r = mid;
            }
            else if (pivot > nums[r])
            {
                l = mid + 1;
            }
            else 
            {
                r -= 1;
            }
        }
        return nums[l];
    }
};