// https://leetcode-cn.com/problems/stone-game-v/

class Solution 
{
    #define N 505

    int prefixSum[N];
    int cut[N][N];
    int dp[N][N];
    int maxL[N][N];
    int maxR[N][N];
    int n;
public:
    vector<int> *sv;
    inline int sum(int l, int r)
    {
        return prefixSum[r] - prefixSum[l] + (*sv)[l];
    }
    int stoneGameV(vector<int>& stoneValue) 
    {
        sv = &stoneValue;
        memset(maxL, 0, N * N * 4);
        memset(maxR, 0, N * N * 4);
        memset(dp, 0, N * N * 4);
        n = stoneValue.size();
        prefixSum[0] = stoneValue[0];
        for (int i = 1; i < n; i++)
        {
            prefixSum[i] = prefixSum[i - 1] + stoneValue[i];
            
        }
        for (int l = 0; l < n; l++)
        {
            
            cut[l][l] = l;
            int cur = l;
            maxL[l][l] = stoneValue[l];
            maxR[l][l] = stoneValue[l];
            for (int r = l + 1; r < n; r++)
            {
                //printf("l: %d, r: %d, cur: %d\n", l, r, cur);
                while (cur < r && sum(l, cur) < sum(cur + 1, r))
                {
                    cur++;
                }
                //printf("cur = %d\n", cur);
                cut[l][r] = cur;    
            }
            
        }
        /*printf("%d %d\n", sum(0, 3), sum(4, 9));
        for (int l = 0; l < n; l++)
        {
            for (int r = l; r < n; r++)
            {
                printf("cut[%d][%d] = %d\n", l, r, cut[l][r]);   
            }
        }*/

        // maxL和maxR来优化转移, 不好想. 
        // 这样就相当于先分治再dp了, 用cut切开分治, 左右一定对应着不同的情况. 
        for (int len = 1; len <= n; len++)
        {
            for (int l = 0; l + len - 1 < n; l++)
            {
                int r = l + len - 1;
                //printf("%d %d\n", l, r);   
                if (cut[l][r] < r && sum(l, cut[l][r]) == sum(cut[l][r] + 1, r))
                {
                    
                    dp[l][r] = max(maxL[l][cut[l][r]], maxR[cut[l][r] + 1][r]);
                }
                else
                {

                    if (cut[l][r] > l)
                    {
                        dp[l][r] = maxL[l][cut[l][r] - 1];
                    }
                    if (cut[l][r] < r)
                    {
                        dp[l][r] = max(dp[l][r], maxR[cut[l][r] + 1][r]);
                    }
                    // index -1
                    //dp[l][r] = max(maxL[l][cut[l][r] - 1], maxR[cut[l][r]][r]);
                }
                
                if (r > l)
                {
                    maxL[l][r] = max(maxL[l][r - 1], dp[l][r] + sum(l, r));
                    maxR[l][r] = max(maxR[l + 1][r], dp[l][r] + sum(l, r));
                }
            }
        }
        
        //printf("maxL[0][1] = %d\n", maxL[0][1]);
        //printf("dp[0][1] = %d\n", dp[0][1]);
        return dp[0][n - 1];
    }
};