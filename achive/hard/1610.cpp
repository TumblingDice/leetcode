class Solution {
public:
    

    vector<double> theta;

    int n, m;
    double const EPS = 1e-8;
    double PI;
    // check if this 2 points are within the angle of degree
    bool within(double theta1, double theta2, double angle)
    {
        return (theta2 - theta1) <= (angle + EPS);
    }
    int s, ans;
    int visiblePoints(vector<vector<int>>& points, int angle, vector<int>& location) 
    {
        n = points.size();
        for (int i = 0; i < n; i++)
        {
            int x = points[i][0] - location[0];
            int y = points[i][1] - location[1];
            if (x == 0 && y == 0)
            {
                s += 1;
                continue;
            }
            theta.emplace_back(atan2(y, x) * 180 / M_PI);
            //printf("%f\n", theta.back());
        }

        m = theta.size();

        sort(theta.begin(), theta.end());


        for (int i = 0; i < m; i++)
        {
            theta.emplace_back(theta[i] + 360);
        }


        /*
        printf("angle: %f\n", theta_angle);
        printf("%d points\n", m);
        for (int i = 0; i < m; i++)
        {
            printf("%f %f\n", theta[i], theta[i] / PI);
        }*/
        int j = 1;
        int ans = 0;
        for (int i = 0; i < m; i++)
        {
            while (j < 2 * m && within(theta[i], theta[j], angle))
            {
                j++;
            }
            //printf("(%d, %d)\n", i, j);
            ans = max(ans, j - i);
        }


        ans += s;
        return ans;
    }
}; 