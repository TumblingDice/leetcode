// https://leetcode-cn.com/problems/fancy-sequence/
class Fancy {
public:
    vector<int> num;
    int factor;
    int offset;
    int mod;
    Fancy() 
    {
        factor = 1;
        offset = 0;
        mod = 1000000007;
    }
    int pow(int x, int y)
    {
        int p = 1;
        int s = x;
        while (y)
        {
            if (y & 1)
            {
                p = (long long) p * s % mod;
            }
            s = (long long) s * s % mod;
            y >>= 1;
        }
        return p;
    }
    int inv(int x)
    {
        return pow(x, mod - 2);
    }
    void append(int val) 
    {
        // factor * x + offset = val;
        // x = (val - offset) / factor;
        //long long tmp = (val - offset) / factor;
        num.push_back((long long) ((val - offset + mod) % mod) * inv(factor) % mod);
    }
    
    void addAll(int inc) 
    {
        offset = (offset + inc) % mod;
    }
    
    void multAll(int m) 
    {
        factor = (long long) factor * m % mod;
        offset = (long long) offset * m % mod;
    }
    
    int getIndex(int idx) 
    {
        if (idx >= num.size())
        {
            return -1;
        }
        int ret = ((long long )((long long ) factor * num[idx] % mod + offset)) % mod;
        return ret;
    }
};

/**
 * Your Fancy object will be instantiated and called as such:
 * Fancy* obj = new Fancy();
 * obj->append(val);
 * obj->addAll(inc);
 * obj->multAll(m);
 * int param_4 = obj->getIndex(idx);
 */
