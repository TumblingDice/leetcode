// https://leetcode-cn.com/problems/dungeon-game/
#include <climits>
class Solution {
public:
    vector<vector<int> > dp;        // dp[x][y] 进入(x, y)之前, 至少需要多少hp

    int transfer[2][2];
    vector<vector<int>> dungeon;

    bool valid(int x[2])
    {
        return (x[0] >= 0) && (x[1] >= 0) && (x[0] < dungeon.size()) && (x[1] < dungeon[0].size());
    }
    int solve(int x[2])
    {
        
        if (dp[x[0]][x[1]] != -1)
        {
            return dp[x[0]][x[1]];
        }

        int res = INT_MAX;
        for (int i = 0; i < 2; i++)
        {
            int nextX[2];
            for (int j = 0; j < 2; j++)
            {
                nextX[j] = x[j] + transfer[i][j];
            }
            if (valid(nextX))
            {
                res = min(res, solve(nextX));
            }
            
        }

        res -= dungeon[x[0]][x[1]];
        res = max(1, res);
        dp[x[0]][x[1]] = res;
        //printf("(%d, %d) returns %d\n", x[0], x[1], res);
        return res;

    }
    int calculateMinimumHP(vector<vector<int>>& dungeon) 
    {
        // shit
        this->dungeon = dungeon;
        for (int i = 0; i <= dungeon.size(); i++)
        {
            dp.push_back(vector<int>(dungeon[0].size() + 1, -1));
        }

        transfer[0][0] = 0;
        transfer[0][1] = 1;
        transfer[1][0] = 1;
        transfer[1][1] = 0;

        dp[dungeon.size() - 1][dungeon[0].size() - 1] = max(1, 1 - dungeon[dungeon.size() - 1][dungeon[0].size() - 1]);
        int initState[2] = {0, 0};
        int ans = solve(initState);
        return ans;
    }
};