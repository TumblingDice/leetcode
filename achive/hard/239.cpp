// https://leetcode-cn.com/problems/sliding-window-maximum/
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) 
    {
        multiset<int> s;
        vector<int> ret;
        for (int i = 0; i < k; i++)
        {
            s.insert(nums[i]);
        }
        ret.push_back(*(s.rbegin()));
        int len = nums.size();
        for (int i = k; i < len; i++)
        {
            auto it = s.find(nums[i - k]);
            s.erase(it);
            s.insert(nums[i]);
            ret.push_back(*(s.rbegin()));
        }


        return ret;
    }
};