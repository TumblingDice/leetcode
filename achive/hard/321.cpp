// https://leetcode-cn.com/problems/create-maximum-number/


class Solution 
{
public:

    vector<int> grek(const vector<int> &num, int k)
    {
        vector<int> ret;
        for (int i = 0; i < num.size(); i++)
        {
            if (num.size() - i == k - ret.size())
            {
                for (int j = i; j < num.size(); j++)
                {
                    ret.push_back(num[j]);
                }
                break;
            }
            while ((ret.size() > 0) && (ret.back() < num[i]) && (num.size() - i > k - ret.size()))
            {
                ret.pop_back();
            }
            if (ret.size() < k)
            {
                ret.push_back(num[i]);
            }
        }

        return ret;

    }

    vector<int> merge(const vector<int>& nums1, const vector<int>& nums2)
    {
        vector<int> ret;
        int i = 0, j = 0;
        while (i < nums1.size() && j < nums2.size())
        {
            if (nums1[i] > nums2[j])
            {
                goto choose_i;
            }
            else if (nums1[i] < nums2[j])
            {
                goto choose_j;
            }
            else 
            {
                for (int l = 0; i + l < nums1.size() && j + l < nums2.size(); l++)
                {
                    if (nums1[i + l] > nums2[j + l])
                    {
                        goto choose_i;
                    }
                    else if (nums1[i + l] < nums2[j + l])
                    {
                        goto choose_j;
                    }
                }
                if (nums1.size() - i > nums2.size() - j)
                {
                    goto choose_i;
                }
                else 
                {
                    goto choose_j;
                }   
            }
            choose_i:
            {
                ret.push_back(nums1[i]);
                i++;
                continue;
            }
            choose_j:
            {
                ret.push_back(nums2[j]);
                j++;
                continue;
            }
        }
        while (i < nums1.size())
        {
            ret.push_back(nums1[i]);
            i++;
        }
        while (j < nums2.size())
        {
            ret.push_back(nums2[j]);
            j++;
        }
        return ret;
    }
    bool geq(const vector<int> &nums1, const vector<int>& nums2)
    {
        for (int i = 0; i < nums1.size(); i++)
        {
            if (nums1[i] > nums2[i])
            {
                return true;
            }
            else if (nums1[i] < nums2[i])
            {
                return false;
            }
        }

        return true;
    }
    vector<int> maxNumber(vector<int>& nums1, vector<int>& nums2, int k) 
    {
        
    
        

        vector<int> ans;
        

        for (int i = 0; i <= k; i++)
        {
            if (i <= nums1.size() && k - i <= nums2.size())
            {
                
                vector<int> res1 = grek(nums1, i);
                vector<int> res2 = grek(nums2, k - i);
                vector<int> res = merge(res1, res2);
                if (ans.size() == 0)
                {
                    ans = res;
                }
                else if (!geq(ans, res))
                {
                    ans = res;
                }
            }
            
        }


        return ans;
    }
};