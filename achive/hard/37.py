# https://leetcode-cn.com/problems/sudoku-solver/

import copy
import sys

class Solution:


    
        
    
                
                
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        def search() -> int:
            def find_min():
                min_size = 10
                min_x = -1
                min_y = -1
                for x in range(9):
                    for y in range(9):
                        if board[x][y] != ".":
                            continue
                        u = row[x] & column[y] & block[x // 3][y // 3]
                        '''if (len(u)) == 0:
                            continue'''
                        if len(u) == 1:
                            return (x, y)
                        if len(u) < min_size:
                            min_x = x
                            min_y = y
                            min_size = len(u)
                if min_size != 10:
                    return (min_x, min_y)
                else:
                    return (-1, -1)

            (x, y) = find_min()
            if (x, y) == (-1, -1):
                
                return 1


            for cand in (row[x] & column[y] & block[x // 3][y // 3]):
                
                board[x][y] = str(cand)


                row[x].discard(cand)
                column[y].discard(cand)
                block[x // 3][y // 3].discard(cand)
                
                res = search()
                if res == 1:
                    return 1
                
                board[x][y] = "."
                row[x].add(cand)
                column[y].add(cand)
                block[x // 3][y // 3].add(cand)

            return 0
        row = []
        column = []
        block = []
        
        tmp = set()
        for i in range(1, 10):
            tmp.add(i)
        for i in range(9):
            row.append(copy.deepcopy(tmp))
            column.append(copy.deepcopy(tmp))
        for x in range(3):
            block.append([])
            for y in range(3):
                block[x].append(copy.deepcopy(tmp))        
        for x in range(9):
            for y in range(9):
                if board[x][y] != ".":
                    tmp = int(board[x][y])
                    row[x].discard(tmp)
                    column[y].discard(tmp)
                    block[x // 3][y // 3].discard(tmp)


        search()

        return 