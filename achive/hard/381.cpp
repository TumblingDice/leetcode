// https://leetcode-cn.com/problems/insert-delete-getrandom-o1-duplicates-allowed/

class RandomizedCollection {
public:

    // STL的hash structure
    unordered_map<int, unordered_set<int>> hash_table;           // value of int -> set of indice of that value in pool
    vector<int> pool;
    //int l, r;
    /** Initialize your data structure here. */
    RandomizedCollection() 
    {
        //r = -1;
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    bool insert(int val) 
    {
        //printf("insert\n");
        pool.push_back(val);
        //r++;
        //pool[r] = val;
        //swap(pool.back(), pool[r]);
        hash_table[val].insert(pool.size() - 1);
        
        return (hash_table[val].size() == 1);
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    bool remove(int val) 
    {
        

        if (hash_table.find(val) == hash_table.end())
        {
            return false;
        }
        //printf("remove %d\n", val);
        int idx = *(hash_table[val].begin());
        //swap(pool[idx], pool.back());
        pool[idx] = pool.back();
        hash_table[val].erase(idx);
        //printf("?: %d\n", (hash_table.find(val) != hash_table.end()));
        hash_table[pool[idx]].erase(pool.size() - 1);
        

        //printf("idx: %d, pool size: %d\n", idx, pool.size());
        if (idx < pool.size() - 1)
        {
            hash_table[pool[idx]].insert(idx);
        }
        if (hash_table[val].size() == 0)
        {
            hash_table.erase(val);
        }
        
        //r--;
        pool.pop_back();
        return true;
    }
    
    /** Get a random element from the collection. */
    int getRandom() 
    {
        // wonder how this is tested
        //printf("random\n");
        return pool[rand() % pool.size()];
    }
};

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection* obj = new RandomizedCollection();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */