// https://leetcode-cn.com/problems/median-of-two-sorted-arrays/
class Solution {
public:

    int findKth(vector<int>& nums1, vector<int>& nums2, int start1, int start2, int k)
    {
        if (start1 >= nums1.size())
        {
            return nums2[start2 + k - 1];
        }
        if (start2 >= nums2.size())
        {
            return nums1[start1 + k - 1];
        }

        if (k == 1)
        {
            return min(nums1[start1], nums2[start2]);
        }

        // 题解这里用的INT_MAX比较鸡贼
        int mid1 = INT_MAX;
        int mid2 = INT_MAX;
        if (start1 + k / 2 - 1 < nums1.size())
        {
            mid1 = nums1[start1 + k / 2 - 1];
        }
        if (start2 + k / 2 - 1 < nums2.size())
        {
            mid2 = nums2[start2 + k / 2 - 1];
        }
        if (mid1 < mid2)
        {
            return findKth(nums1, nums2, start1 + k / 2, start2, k - k / 2);
        }
        else 
        {
            return findKth(nums1, nums2, start1, start2 + k / 2, k - k / 2);
        }

    }
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) 
    {
        int n1 = nums1.size();
        int n2 = nums2.size();
        
        int l = (n1 + n2 + 1) / 2;
        int r = (n1 + n2 + 2) / 2;
        printf("%d %d\n", findKth(nums1, nums2, 0, 0, l), findKth(nums1, nums2, 0, 0, r));
        return (findKth(nums1, nums2, 0, 0, l) + findKth(nums1, nums2, 0, 0, r)) / 2.0;
    }
};