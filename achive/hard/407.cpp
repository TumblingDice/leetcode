// https://leetcode-cn.com/problems/trapping-rain-water-ii/


// 形式上是一个线性规划问题, 
// 使水量最大化
// 限制是每个格子的水面高度要小于等于相邻格子的水面高度.
// 并且边界上的水面高度为格子高度.

// 核心是从边界出发.
class Solution {
public:
    struct Node
    {
        int x[2];
        int height;
        Node (int x, int y, int h)
        {
            this->x[0] = x;
            this->x[1] = y;
            this->height = h;
        }
    };
    bool visit[200][200];
    int direction[2][2];

    struct cmp
    {
        bool operator () (const Node& a, const Node& b)
        {
            return a.height > b.height;
        }
    };
    int trapRainWater(vector<vector<int>>& heightMap) 
    {
        int m = heightMap.size();
        int n = heightMap[0].size();
        memset(visit, 0, sizeof(visit));
        priority_queue<Node, vector<Node>, cmp> pq;
        for (int j = 1; j < n - 1; j++)
        {
            pq.push(Node(0, j, heightMap[0][j]));
            pq.push(Node(m - 1, j, heightMap[m - 1][j]));
            visit[0][j] = true;
            visit[m - 1][j] = true;
        }

        for (int i = 0; i < m; i++)
        {
            pq.push(Node(i, 0, heightMap[i][0]));
            pq.push(Node(i, n - 1, heightMap[i][n - 1]));
            visit[i][0] = true;
            visit[i][n - 1] = true;
        }
        int ans = 0;
        direction[0][0] = 1;
        direction[0][1] = -1;
        direction[1][0] = 1;
        direction[1][1] = -1;
        while (!pq.empty())
        {
            Node t = pq.top();
            //printf("(%d, %d) height = %d\n", t.x[0], t.x[1], t.height);
            pq.pop();
            
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    int x[2] = {t.x[0], t.x[1]};
                    x[i] += direction[i][j];

                    if (x[0] < 0 || x[0] >= m || x[1] < 0 || x[1] >= n || visit[x[0]][x[1]])
                    {
                        continue;
                    }
                    
                    if (heightMap[x[0]][x[1]] < t.height)
                    {
                        ans += (t.height - heightMap[x[0]][x[1]]);
                    }
                    Node newNode(x[0], x[1], max(heightMap[x[0]][x[1]], t.height));
                    pq.push(newNode);
                    visit[x[0]][x[1]] = true;
                }
                
            }
        }



        return ans;
    }
};                          