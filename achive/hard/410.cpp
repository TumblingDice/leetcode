// https://leetcode-cn.com/problems/split-array-largest-sum/

#include <cstdio>
#include <cstring>
#include <vector>
#include <cstdlib>
#include <cmath>
using namespace std;

class Solution 
{
public:
    int splitArray(vector<int>& nums, int m) 
    {
        int n = nums.size();
        unsigned prefix[1001];
        int s = 0;
        unsigned int dp[1001][51];


        memset(dp, -1, sizeof(dp));
        for (int i = 0; i <= n; i++)
        {
            prefix[i] = s;
            s += nums[i];
        }

        for (int i = 0; i < n; i++)
        {
            dp[i][0] = prefix[i + 1];
        }

        /*for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                printf("%d ", dp[j][i]);
            }
            printf("\n");
        }
        printf("\n");printf("\n");
        for (int i = 0; i <= n; i++)
        {
            printf("%d ", prefix[i]);
        }
        printf("\n");printf("\n");*/

        for (int j = 0; j < m - 1; j++)
        {

            for (int i = 0; i < n; i++)
            {
                for (int k = i + 1; k < n; k++)
                {   // i
                    dp[k][j + 1] = min(dp[k][j + 1], max(dp[i][j], (prefix[k + 1] - prefix[i + 1])));
                }
            }
        }

        /*for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                printf("%d ", dp[j][i]);
            }
            printf("\n");
        }
        printf("\n");printf("\n");*/
        return dp[n - 1][m - 1];

        
    }
};

/*
int main()
{
    Solution s;


    vector<int> v;
    v.push_back(7);
    v.push_back(2);
    v.push_back(5);
    v.push_back(10);
    v.push_back(8);

    printf("%d\n", s.splitArray(v, 2));

    return 0;
}*/