// https://leetcode-cn.com/problems/trapping-rain-water/

class Solution 
{
public:
    struct Node
    {
        int height;
        int x;
        int next;
        Node(int x, int h)
        {
            this->x = x;
            this->height = h;
            this->next = -1;
        }
    };
    int trap(vector<int>& height) 
    {
        stack<Node> s;
        int ans = 0;
        int n = height.size();
        for (int i = 0; i < n; i++)
        {
            while ((!s.empty()) && (s.top().height <= height[i]))
            {
                Node t = s.top();
                s.pop();
                ans += (i - t.x - 1) * (t.height - t.next);
                //printf("node#%d popping node#%d, height %d, next %d, distance %d\n", i, t.x, t.height, t.next, (i - t.x - 1));
            }
            if (!s.empty())
            {
                ans += (i - s.top().x - 1) * (height[i] - s.top().next);
                s.top().next = height[i];
            }
            s.push(Node(i, height[i]));
        }


        return ans;
    }
};


/*
     1
1    1
1  1 1
11 111
11 111
*******/