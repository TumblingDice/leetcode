// https://leetcode-cn.com/problems/reverse-pairs/
class Solution {
public:
    
    long long buf[100000 + 5];
    long long a[100000 + 5];
    int n;
    int ans;
    void sort(vector<int>& a, int l, int r)
    {
        if (l == r)
        {
            return;
        }
        int mid = (l + r) / 2;
        sort(a, l, mid);
        sort(a, mid + 1, r);
        int i = l, j = mid + 1;
        int c = l;

        int tmp = 0;
        
        while (i <= mid && j <= r)
        {
            if (a[i] <= a[j])
            {
                buf[c] = a[i];
                i++;
            }
            else
            {
                buf[c] = a[j];
                j++;

            }
            c++;
        }
        while (i <= mid)
        {
            buf[c] = a[i];
            i++;
            c++;

        }
        while (j <= r)
        {
            buf[c] = a[j];
            j++;
            c++;
        }


        i = l;
        j = mid + 1;

        for (j = mid + 1; j <= r; j++)
        {
            while (i <= mid && ((long long) a[i] <= (long long) a[j] * 2))
            {
                i++;
            }
            tmp += (mid + 1 - i);
        }
        ans += tmp;
        for (int k = l; k <= r; k++)
        {
            a[k] = buf[k];
        }
        //printf("l: %d, mid: %d, r: %d, tmp: %d\n", l, mid, r, tmp);
        return; 
    }
    int reversePairs(vector<int>& nums) 
    {
        ans = 0;
        
        n = nums.size();
        if (n == 0)
        {
            return 0;
        }
        sort(nums, 0, n - 1);
        
        return ans;
    }
};
