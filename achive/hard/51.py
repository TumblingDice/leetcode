# https://leetcode-cn.com/problems/n-queens/
import copy
class Solution(object):

    def __init__(self):
        self.buffer = []
        self.ans = []
        self.path = []
        self.n = 0
    def makeStr(self, x):
        return ("." * x + "Q" + "." * (self.n - x - 1)) 
    def dfs(self, depth):
        
        if depth == self.n:
            self.ans.append(copy.deepcopy(self.buffer))
            return 
            
        for i in range(0, self.n):
            
            ok = True
            for j in range(0, depth):
                if (i == self.path[j]) or (depth - j == i - self.path[j]) or (depth - j == self.path[j] - i):
                    ok = False
                    break

            if not ok:
                continue
            self.path.append(i)
            self.buffer[depth] = self.makeStr(i)
            self.dfs(depth + 1)
            self.path.pop()
        return 




    def solveNQueens(self, m):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        self.n = m
        self.buffer = [0] * self.n
        self.dfs(0)
        return self.ans



'''
if __name__ == "__main__":
    s = Solution()

    print(s.solveNQueens(4))
'''