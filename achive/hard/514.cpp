// https://leetcode-cn.com/problems/freedom-trail/


class Solution {
public:
    typedef pair<int, int> State;       // (where am I, what's next)
    string ring;
    string key;

    int dp[101][101];

    int solve(int rPos, int kPos)
    {
        if (dp[rPos][kPos] != -1)
        {
            return dp[rPos][kPos];
        }
        State nextState;

        int p = rPos;
        int count = 0;
        while (ring[p] != key[kPos])
        {
            if (p == 0)
            {
                p = ring.length() - 1;
            }
            else 
            {
                p--;
            }
            count++;
        }
        dp[rPos][kPos] = solve(p, kPos + 1) + count + 1;
        p = rPos;
        count = 0;
        while (ring[p] != key[kPos])
        {
            if (p == ring.length() - 1)
            {
                p = 0;
            }
            else 
            {
                p++;
            }
            count++;
        }
        dp[rPos][kPos] = min(dp[rPos][kPos], solve(p, kPos + 1) + count + 1);

        return dp[rPos][kPos];
    }


    int findRotateSteps(string ring, string key) 
    {
        this->ring = ring;
        this->key = key;

        // 这题的模型根本上是给定两点的最短路
        // 但这个图的结构比较有特殊, 是层次化的.
        memset(dp, -1, 101 * 101 * 4);
        for (int i = 0; i < ring.length(); i++)
        {
            dp[i][key.length()] = 0;
        }


        int ans = solve(0, 0);
        return ans;
    }
};