// https://leetcode-cn.com/problems/insert-interval/

#include <vector>
#include <cstdio>

class Interval
{
public:
    int left, right;

    bool operator < (const Interval &that) const
    {
        return this->left < that.left;
    }

    bool operator == (const Interval &that) const
    {
        return this->left == that.left;
    }
    bool operator != (const Interval &that) const
    {
        return this->left != that.left;
    }
    

    bool operator > (const Interval &that) const
    {
        return this->left > that.left;
    }

    Interval(int l, int r)
    {
        left = l;
        right = r;
    }
    Interval() {}
};





template <typename Data> class Node
{
public:
    Data data;
    Node *child[2];
    Node *father;
    Node(const Data& d)
    {
        father = nullptr;
        child = {nullptr, nullptr};
        this->data = d;
    }
};





template <typename Data> class BST
{
public:
    virtual Node<Data>* insert(const Data&) = 0;
    virtual Node<Data>* find(const Data&) = 0;
    virtual void erase(const Data&) = 0;
    ~BST()      // deconstruction slows it down in leetcode.
    {

    }

};


template <typename Data> class Splay: 
    public BST<Data>
{
public:
    
    Node<Data>* find(const Data& d)
    {
        Node<Data> *t = root;
        while (t != nullptr)
        {
            if (d == t->data)
            {
                
                splay(nullptr, t);
                root = t;
                return t;
            }
            else if (d > t->data)
            {
                if (t->child[1] == nullptr)
                {
                    //splay(nullptr, t);
                    return t;
                }
                t = t->child[1];
            }
            else 
            {
                if (t->child[0] == nullptr)
                {
                    //splay(nullptr, t);
                    return t;
                }
                t = t->child[0];
            }
        }

        return nullptr;
    }
    int rank(const Data& d)
    {
        Node<Data> *res = find(d);
        if (res == nullptr)
        {
            return -1;
        }
        if (res->data != d)
        {
            return -1;
        }
        return subTreeSize(res->child[0]) + 1;
    }
    Node<Data>* insert(const Data& d)
    {
        Node<Data> *res = find(d);
        
        if (res == nullptr)
        {
            Node<Data> *newNode = getNewNode(d);
            root = newNode;
            newNode->father = nullptr;
            return root;
        }
        if (res->data == d)
        {
            return root;
        }
        Node<Data> *newNode = getNewNode(d);
        int isRightChild = (res->data < d);
        res->child[isRightChild] = newNode;
        newNode->father = res;
        splay(nullptr, newNode);
        root = newNode;
        return root;
    }

    Node<Data>* findPrior(const Data& d)
    {
        Node<Data>* res = _findPrior(root, d);
        if (res != nullptr)
        {
            splay(nullptr, res);
            root = res;
        }
        
        return res;
    }

    void erase(const Data& d)
    {
        Node<Data> *res = find(d);
        
        if (res == nullptr)
        {
            return;
        }
        if (res->data != d)
        {
            return;
        }

        
        if (res->child[0] == nullptr)
        {
            root = res->child[1];
            res->child[1]->father = nullptr;
            return;
        }
        
        
        res = _findPrior(root->child[0], d);
        
        splay(root, res);
        res->child[1] = root->child[1];
        root->child[1]->father = res;
        res->father = nullptr;
        root = res;
    }

    void print()
    {
        _print(root, 0);
    }
    

    Splay() 
    {
        this->root = nullptr;
    }

    int rank()
private:
    Node<Data>* getNewNode(const Data& d)
    {
        #ifdef USE_NEW
        return new Node<Data> (d);
        #else
        nodes[watermark].data = d;
        return &(nodes[watermark++]);
        #endif
    }
    void rotateSingle(Node<Data>* t)
    {
        int isLeftChild = (t->father->child[0] == t);
        Node<Data> *f = t->father;

        if (t->child[isLeftChild] != nullptr)
        {
            t->child[isLeftChild]->father = f;
        }
        f->child[!isLeftChild] = t->child[isLeftChild];
        t->father = f->father;
        f->father = t;
        t->child[isLeftChild] = f;

        if (t->father != nullptr)
        {
            if (t->father->child[0] == f)
            {
                t->father->child[0] = t;
            }
            else
            {
                t->father->child[1] = t;
            }
        }
    }

    void rotateDouble(Node<Data>* t)

    {
        Node<Data> *f = t->father, *gf = t->father->father;
        
        int isLeftChild = (f->child[0] == t);

        f->child[!isLeftChild] = t->child[isLeftChild];
        gf->child[isLeftChild] = t->child[!isLeftChild];
        t->father = gf->father;
        f->father = t;
        gf->father = t;
        t->child[isLeftChild] = f;
        t->child[!isLeftChild] = gf;
        

        if (t->father != nullptr)
        {
            if (t->father->child[0] == gf)
            {
                t->father->child[0] = t;
            }
            else
            {
                t->father->child[1] = t;
            }
        }
    }

    void splay(Node<Data> *f, Node<Data> *t)
    {   
        //int flag = (t->data.left == 4 && t->father->data.left == 8);
        while (t->father != f)
        {
            if (t->father->father == f)
            {
                rotateSingle(t);
                break;
            }
            else
            {
                int isLeftChild1 = (t->father->father->child[0] == t->father);
                int isLeftChild2 = (t->father->child[0] == t);
                if (isLeftChild1 != isLeftChild2)
                {
                    
                    rotateDouble(t);
                }
                else
                {
                    rotateSingle(t->father);
                    rotateSingle(t);
                }
            }
        }
        //root = t;
    }

    Node<Data>* _findPrior(Node<Data> *t, const Data& d)
    {
        if (t == nullptr)
        {
            return nullptr;
        }
        if (d == t->data)
        {
            return t;
        }
        else if (d > t->data)
        {
            Node<Data> *res = _findPrior(t->child[1], d);
            if (res == nullptr)
            {
                return t;
            }
            else 
            {
                return res;
            }
        }
        else if (d < t->data)
        {
            Node<Data> *res = _findPrior(t->child[0], d);
            return res;
        }

        return nullptr;
    }

    void _print(Node<Data> *t, int depth)
    {
        if (t == nullptr)
        {
            printf("dep: %d\tnul\n", depth);
            return;
        }
        printf("dep: %d\tinterval: %d %d\tfather: %d\n", depth, t->data.left, t->data.right, (t->father == nullptr) ? (-1) : (t->father->data.left));

        _print(t->child[0], depth + 1);
        _print(t->child[1], depth + 1);

        return;

    }

    int subTreeSize(Node<data> *t)
    {
        if (t == nullptr)
        {
            return 0;
        }
        return subTreeSize(t->child[0]) + subTreeSize(t->child[1]) + 1;
    }

    Node<Data> *root;

    #ifndef USE_NEW
    Node<Data> nodes[1e5 + 5];
    int watermark = 0;
    #endif
};


/*
using namespace std;
class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval)
    {
        Splay<Interval> splay;
        for (int i = 0; i < intervals.size(); i++)
        {
            Interval r(intervals[i][0], intervals[i][1]);
            splay.insert(r);
        }

        
    }
};*/


int main()
{
    /*Splay <Interval> splay;
    Interval r(1, 2);
   // Node<Interval> *node = new Node <Interval> (r);

    //printf("%d %d\n", node->data.left, node->data.right);
    splay.insert(r);
    
    r.left = 3; r.right = 5;
    splay.insert(r);


    r.left = 6; r.right = 7;
    splay.insert(r);

    r.left = 8; r.right = 10;
    splay.insert(r);

    r.left = 12; r.right = 16;
    splay.insert(r);

    r.left = 4; r.right = 8;
    splay.insert(r);


    r.left = 11; r.right = 100;
    auto res = splay.findPrior(r);
    if (res != nullptr)
    {
        printf("\t%d %d\n", res->data.left, res->data.right);
    }
    //splay.print();

    r.left = 4; r.right = 12314;
    splay.erase(r);
    
    r.left = 6; r.right = 12314;
    splay.erase(r);
    
    r.left = 8; r.right = 12314;
    splay.erase(r);
   //splay.print();
    splay.print();*/
    Splay<int> splay;
    int n;
    scanf("%d",  &n);
    for (int i = 0; i < n; i++)
    {
        int op, x;
        scanf("%d%d", &op, &x);
        switch (op)
        {
            case (1):
            {
                splay.insert(x);
                break;
            }
            case (2):
            {
                splay.erase(x);
            }
            case (3):
            {
                splay.
            }
        }
    }

    return 0;
}





