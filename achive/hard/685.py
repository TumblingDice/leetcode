# https://leetcode-cn.com/problems/redundant-connection-ii/
class Solution:
    def findRedundantDirectedConnection(self, edges: List[List[int]]) -> List[int]:

        ancestor = [x for x in range(1005)]
        father = [x for x in range(1005)]

        adj = [[] for x in range(1005)]
        conflict = [-1, -1]
        circle = [-1, -1]

        def findAncestor(x: int) -> int:
            if ancestor[x] == x:
                return x
            ancestor[x] = findAncestor(ancestor[x])
            return ancestor[x]

        def union(x: int, y: int) -> None:
            ancestor[findAncestor(x)] = findAncestor(y)

        def hasCircle(u: int) -> bool:
            if u == circle[0]:
                return True
            
            for v in adj[u]:
                if hasCircle(v) == True:
                    return True
            
            return False

        for [u, v] in edges:
            adj[u].append(v)
            if father[v] != v:
                conflict = [u, v]
            else:
                father[v] = u
            
                fu = findAncestor(u)
                fv = findAncestor(v)
                
                if fu == fv:
                    circle = [u, v]
                else:
                    ancestor[fv] = fu
        
        #print(circle)
        #print(conflict)
        if conflict != [-1, -1] and circle != [-1, -1]:
            #print([father[conflict[1]], conflict[1]])
            adj[father[conflict[1]]].remove(conflict[1])
            #print(adj[father[conflict[1]]])
            for v in adj[circle[0]]:
                if hasCircle(v):
                    return conflict
            return [father[conflict[1]], conflict[1]]
                
        elif conflict != [-1, -1]:
            return conflict
        elif circle != [-1, -1]:
            return circle



        
        return [-1, -1]