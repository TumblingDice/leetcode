// https://leetcode-cn.com/problems/cherry-pickup/


// 稍微转变一下就跟上一题(1463)一样了
class Solution {
public:
    int dp[100][100][100];     // dp[x1][x2][y1]  : 第一个人在(x1, y1), 第二个人在(x2, y2 = x1 - x2 + y1)之后的最优解

    int transfer[2][2];
    int n;
    //int possible;
    vector<vector<int>> *g;
    inline bool valid(int x1, int x2, int y1, int y2)
    {
        return (x1 < n && x1 >= 0 && y1 < n && y1 >= 0 && x2 < n && x2 >= 0 && y2 < n && y2 >= 0
                && ((*g)[x1][y1] != -1) && ((*g)[x2][y2] != -1));
    }
    int solve(int x1, int x2, int y1)
    {
        
        int y2 = x1 + y1 - x2;
        
        //printf("(%d, %d) and (%d, %d)\n", x1, y1, x2, y2);
        //possible |= (x1 == n - 1 && y1 == n - 1);
        if (dp[x1][x2][y1] != -1)
        {
            return dp[x1][x2][y1];
        }
        
        int res = -12312123;        // just some trap value to mark a unreachable state
        // transfer
        //printf("(%d, %d) and (%d, %d)\n", x1, y1, x2, y2);
        for (int i = 0; i < 2; i++)
        {
            int nextX1 = x1 + transfer[i][0];
            int nextY1 = y1 + transfer[i][1];
            for (int j = 0; j < 2; j++)
            {
                int nextX2 = x2 + transfer[j][0];
                int nextY2 = y2 + transfer[j][1];
                if (valid(nextX1, nextX2, nextY1, nextY2))
                {
                    res = max(res, solve(nextX1, nextX2, nextY1));
                }
            }
        }
        //printf("(%d, %d) and (%d, %d)\n", x1, y1, x2, y2);
        if (x1 == x2)
        {
            res += (*g)[x1][y1];
        }
        else
        {
            res += (*g)[x1][y1] + (*g)[x2][y2];
        }
        dp[x1][x2][y1] = res;
        //printf("(%d, %d) (%d, %d) return %d\n", x1, y1, x2, y2, res);
        return res;
    }
    
    

    int cherryPickup(vector<vector<int>>& grid) 
    {
        g = &grid;

        transfer[0][0] = 0;
        transfer[0][1] = 1;
        transfer[1][0] = 1;
        transfer[1][1] = 0;
        memset(dp, -1, 100 * 100 * 100 * 4);
        //possible = false;
        n = grid.size();
        dp[n - 1][n - 1][n - 1] = grid[n - 1][n - 1];
        
        int ans = solve(0, 0, 0);
        //return (possible) ? ans : 0;
        return max(0, ans);
    }
};