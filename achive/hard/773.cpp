class Solution {
public:


/*
0 1 2
3 4 5
*/
    int exp[6] = {1, 10, 100, 1000, 10000, 100000}; 
    int visit[666666];
    int sol[666666];  
    #define EXP(k) (exp[k])
    #define KTH_DIGIT(num, k) ((num / EXP(k)) % 10)

    // swap x-th digit and y-th digit of num.
    #define SWAP_DIGIT(num, xth, yth) (num + EXP(xth) * (KTH_DIGIT(num, yth) - KTH_DIGIT(num, xth)) + EXP(yth) * (KTH_DIGIT(num, xth) - KTH_DIGIT(num, yth)))

    // swap k-th block upward/downward/leftward/rightward
    // return possitive number if possible
    // return -1 if impossible
    #define SWAP_UP(num, k)     ((k >= 3) ? SWAP_DIGIT( num, k, k - 3) : (-1))
    #define SWAP_DOWN(num, k)   ((k <= 2) ? SWAP_DIGIT(num, k, k + 3) : (-1))
    #define SWAP_LEFT(num, k)   (((k == 0) || (k == 3)) ? (-1) : SWAP_DIGIT(num, k, k - 1))
    #define SWAP_RIGHT(num, k)  (((k == 2) || (k == 5)) ? (-1) : SWAP_DIGIT(num, k, k + 1))

    #define FINAL_STATE (123450)





    int slidingPuzzle(vector<vector<int>>& board) 
    {

        int initState = 0;
        int s = 1;
        for (int i = 1; i >= 0 ; i--)
        {
            for (int j = 2; j >= 0; j--)
            {
                initState += board[i][j] * s;
                s *= 10;
            }
        }
        if (initState == FINAL_STATE)
        {
            return 0;
        }
        
        memset(visit, 0, sizeof(visit));
        memset(sol, -1, sizeof(sol));
        visit[initState] = 1;
        
        queue< pair<int, int> > q;
        pair<int, int> tmp;
        tmp.first = initState;
        tmp.second = 0;
        q.push(tmp);

        while (!q.empty())
        {
            auto t = q.front();
            q.pop();
            for (int i = 0; i < 6; i++)
            {
                if (KTH_DIGIT(t.first, i) != 0)
                {
                    continue;
                }
                int nextState;

                
                nextState = SWAP_UP(t.first, i);
                if (nextState == FINAL_STATE)
                {
                    return t.second + 1;
                }
                if (nextState != -1 && !visit[nextState])
                {
                    tmp.first = nextState;
                    tmp.second = t.second + 1;
                    visit[nextState] = 1;
                    q.push(tmp);
                }

                nextState = SWAP_LEFT(t.first, i);
                if (nextState == FINAL_STATE)
                {
                    return t.second + 1;
                }
                if (nextState != -1 && !visit[nextState])
                {
                    tmp.first = nextState;
                    tmp.second = t.second + 1;
                    visit[nextState] = 1;
                    q.push(tmp);
                }

                nextState = SWAP_RIGHT(t.first, i);
                if (nextState == FINAL_STATE)
                {
                    return t.second + 1;
                }
                if (nextState != -1 && !visit[nextState])
                {
                    tmp.first = nextState;
                    tmp.second = t.second + 1;
                    visit[nextState] = 1;
                    q.push(tmp);
                }

                nextState = SWAP_DOWN(t.first, i);
                if (nextState == FINAL_STATE)
                {
                    return t.second + 1;
                }
                if (nextState != -1 && !visit[nextState])
                {
                    tmp.first = nextState;
                    tmp.second = t.second + 1;
                    visit[nextState] = 1;
                    q.push(tmp);
                }
            }
        }
        return -1;
    }
};