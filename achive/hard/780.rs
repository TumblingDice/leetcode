// https://leetcode-cn.com/problems/reaching-points/
impl Solution 
{
    pub fn reaching_points(sx: i32, sy: i32, tx: i32, ty: i32) -> bool 
    {
        let mut x = tx;
        let mut y = ty;
        loop
        {
            //print!("({}, {})\n", x, y);
            if (x == sx && y == sy)
            {
                return true;
            }
            if (x < sx || y < sy)
            {
                return false;
            }
            if (x > y)
            {
                if ((x - sx) % y == 0 && y == sy)
                {
                    return true;
                }    
                x = x % y;
            }
            else 
            {
                //print!("y={} sy={}\n", y, sy);
                if ((y - sy) % x == 0 && x == sx)
                {
                    return true;
                }
                y = y % x;
            }
        }        
    }
}