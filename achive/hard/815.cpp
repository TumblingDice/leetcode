// https://leetcode-cn.com/problems/bus-routes/

//稍微转化一下就是顶点带权的最短路问题, 再归约一下就到了边权的最短路.

class Solution 
{
public:
    struct Node
    {
        struct Edge
        {
            int to;
            int weight;
        };

        vector<Edge> neighbor;
        int dist = -1;

        friend bool operator < (Node &x, Node &y)
        {
            return x.dist < y.dist;
        }
    };

    static const int ROUTE_BASE = 1000000;
    Node node[ROUTE_BASE + 100000 + 5];
    
    inline int routeNodeIndex(int x)
    {
        return x + ROUTE_BASE;
    }
    int numBusesToDestination(vector<vector<int>>& routes, int S, int T) 
    {
        for (int i = 0; i < routes.size(); i++)
        {
            for (int j = 0; j < routes[i].size(); j++)
            {
                Node::Edge e;
                e.to = routeNodeIndex(i);
                e.weight = 1;
                node[routes[i][j]].neighbor.push_back(e);
                e.to = routes[i][j];
                e.weight = 0;
                node[routeNodeIndex(i)].neighbor.push_back(e);
            }
        }


        // dijkstra
        memset(dist, -1, sizeof(dist));
        priority_queue<Node*> q;
        dist[S] = 0;
        q.push(&node[S]);

        while (!q.empty())
        {
            Node *u = q.top();
            if (u == node + T)
            {
                return node[T].dist;
            }
            q.pop();
            
            for (Node::Edge e: u->neighbor)
            {
                int v = e.to;
                if (node[v].dist == -1 || node[v].dist > u->dist + e.weight)
                {
                    node[v].dist = u->dist + e.weight;
                    q.push(&node[v]));
                }
            }
        }
        return node[T].dist;
    }
};