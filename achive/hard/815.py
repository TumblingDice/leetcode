#  https://leetcode-cn.com/problems/bus-routes/


# 写的dijkstra居然会超时, 可能用spfa能过. 
# 我感觉数据有问题, 如果python的set能过. 
# 关键是python的set只是用了hash, 所以效率上应该挺rough的
# 也许用STL的bitset会快一点. 
class Solution:
    def numBusesToDestination(self, routes: List[List[int]], S: int, T: int) -> int:
        if S == T:
            return 0
        bus = []
        for route in routes:
            bus.append(set(route))
        
        q = []
        q.append((S, 0))
        visit = {}
        visit[S] = 1

        while (len(q) != 0):
            (t, dist) = q.pop(0)
            for route in bus:
                if t in route:
                    for stop in route:
                        if stop not in visit:
                            if stop == T:
                                return dist + 1
                            q.append((stop, dist + 1))
                            visit[stop] = 1
        
        return -1