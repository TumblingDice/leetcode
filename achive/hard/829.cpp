// https://leetcode-cn.com/problems/consecutive-numbers-sum/

class Solution 
{
public:
    int consecutiveNumbersSum(int N) 
    {
        int n = N * 2;
        int s = sqrt(n);
        int ans = 0;
        for (int i = 1; i <= s; i++)
        {
            if (n % i != 0)
            {
                continue;
            }
            int t = (n / i) - i - 1;
            if (t % 2 != 0)
            {
                continue;
            }
            if (t < 0)
            {
                break;
                //continue;
            }
            ans ++;
        }
        return ans;
    }
};