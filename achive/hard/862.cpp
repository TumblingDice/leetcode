// https://leetcode-cn.com/problems/shortest-subarray-with-sum-at-least-k/
#include <deque>
class Solution 
{
public:
    int shortestSubarray(vector<int>& A, int K) 
    {
        int n = A.size();
        int k = K;
        int l = 0, r = 0;
        int ans = -1;
        int sum = 0;


        vector<int> prefixSum;
        deque<int> d;
        prefixSum.push_back(0);
        for (int i = 0, sum = 0; i < n; i++)
        {
            sum += A[i];
            prefixSum.push_back(sum);
        }



       for (int i = 0; i < prefixSum.size(); i++)
        {
            while ((!d.empty()) && (prefixSum[i] - prefixSum[d.front()] >= k))
            {
                if ((ans == -1) || (ans > (i - d.front())))
                {
                    ans = (i - d.front());
                }
                d.pop_front();
            }
            while ((!d.empty()) && (prefixSum[d.back()] >= prefixSum[i]))
            {
                d.pop_back();
            }
            d.push_back(i);
        }


        return ans;
    }
};


// [84,-37,32,40,95]
// 167
// 0 84 47 79 119 214
// 0 1  2  3   4   5

