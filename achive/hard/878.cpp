// https://leetcode-cn.com/problems/nth-magical-number/submissions/
class Solution 
{
public:
    const int MOD = 1000000007;
    int gcd(int a, int b)
    {
        if (b == 0)
        {
            return a;
        }
        else 
        {
            return gcd(b, a % b);
        }
    }
    int lcm(int a, int b)
    {
        return (a * b) / gcd(a, b); 
    }
    // return the m-th common multiple between [0, lcm(a, b))
    // upper bound: O(a + b)
    int get_mth(int a, int b, int m)
    {
        int k = m;
        int x = a;
        int y = b;
        if (m == 0)
        {
            return 0;
        }
        while (true)
        {
            if (k == 1)
            {
                return min(x, y);
            }
            int skip;
            if (x < y)
            {
                skip = (y - x) / a + 1;
                if (skip >= k)
                {
                    return x + (k - 1) * a;
                }
                x += skip * a;
            }
            else 
            {
                skip = (x - y) / b + 1;
                if (skip >= k)
                {
                    return y + (k - 1) * b;

                }
                y += skip * b;
            }
            k -= skip;
        }
    }
    int nthMagicalNumber(int n, int a, int b) 
    {
        int x = a / gcd(a, b);
        int y = b / gcd(a, b);
        int count = x + y - 1;
        int m = n % count;
        //printf("%d\n", get_mth(5, 13, 6));
        int res = ((long long)((lcm(x, y) * ((long long)n / count) + get_mth(x, y, m)) * gcd(a, b))) % MOD;
        return res;
    }
};