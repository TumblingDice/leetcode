impl Solution 
{
    
    priv fn gcd(a: i32, b: i32) -> i32
    {
        let mo = 1000000007;
        if (b == 0)
        {
            return a;
        }
        else 
        {
            return gcd(b, a % b);
        }
    }
    priv fn lcm(a: i32, b: i32) -> i32
    {
        let mo = 1000000007;
        return (a * b) / gcd(a, b); 
    }


    // return the m-th common multiple between [0, lcm(a, b))
    priv fn get_mth(a: i32, b: 32, m: i32) -> i32
    {
        let mo = 1000000007;
        let mut k = m;
        let mut x = a;
        let mut y = b;
        if (m == 0)
        {
            return 0;
        }
        loop
        {
            if (k == 1)
            {
                return cmp::min(x, y);
            }
            let skip;
            if (x < y)
            {
                skip = (y - x) / a + 1;
                if (skip >= k)
                {
                    return x + (k - 1) * a;
                }
                x += skip * a;
            }
            else 
            {
                skip = (x - y) / b + 1;
                if (skip >= k)
                {
                    return y + (k - 1) * b;

                }
                y += skip * b;
            }
            k -= skip;
        }
    }

    pub fn nth_magical_number(n: i32, a: i32, b: i32) -> i32 
    {
        let x = a / gcd(a, b);
        let y = b / gcd(a, b);
        let count = x + y - 1;
        let m = n % count;
        
        let res = (lcm(x, y) * (n / count) + get_mth(x, y, m)) * gcd(a, b);
    }
}