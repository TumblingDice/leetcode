
// https://leetcode-cn.com/problems/profitable-schemes/

class Solution {
public:
    int dp[101][101][101];
    // 其实这个dp只用2维. 
    // 这个递归还是很容易变形成递推的.
    int solve(vector<int>& group, vector<int>& profit, int i, int j, int k)
    {
        if (j < 0)
        {
            return 0;
        }
        if (dp[i][j][k] != -1)
        {
            return dp[i][j][k];
        }
        
        dp[i][j][k] = (solve(group, profit, i - 1, j - group[i - 1], max(0, k - profit[i - 1])) + solve(group, profit, i - 1, j, k)) % (1000000007);
        return dp[i][j][k];
    }
    int profitableSchemes(int G, int P, vector<int>& group, vector<int>& profit) 
    {
        
        memset(dp, -1, 101 * 101 * 101 * 4);
        for (int j = 0; j <= G; j++)
        {
            for (int k = 1; k <= P; k++)
            {
                dp[0][j][k] = 0;
            }
            dp[0][j][0] = 1;
        }

        // dp[i][j][k] = 用j个人在前i个犯罪获利大于等于k的方案数

        int ans = solve(group, profit, group.size(), G, P);
        return ans;
    }
};