# https://leetcode-cn.com/problems/equal-rational-numbers/submissions/
import copy
class Solution:

    def form(self, s: str) -> (str, str):
        if (s.find("(") == -1):
            
            return (s, "")
        fixed = s[: s.find("(")]
        loop = s[s.find("(") + 1: s.find(")")]
        return (fixed, loop)
    

    def allEqual(self, p) -> bool:
        for i in range(1, len(p)):
            if (p[i - 1] != p[i]):
                return False
        
        return True

    def contract(self, s: str) -> str:
        for i in range(len(s), 1, -1):
            if len(s) % i != 0:
                continue
            k = len(s) // i
            p = [s[k * j: k * (j + 1)] for j in range(0, i)]
            #print(p)
            if (not self.allEqual(p)):
                continue

            return p[0]
        return s

    # convert an arbitary into a minimium form: a(b) 
    # i.e. any posfix of a is not a posfix of b
    def minimium(self, fixed: str, loop: str) -> (str, str):
        (oldFixed, oldLoop) = copy.deepcopy((fixed, loop))
        (fixed, loop) = self._minimium(fixed, loop)
        while ((fixed, loop) != (oldFixed, oldLoop)):
            (oldFixed, oldLoop) = copy.deepcopy((fixed, loop))
            (fixed, loop) = self._minimium(fixed, loop)
        return (fixed, loop)
    def _minimium(self, fixed: str, loop: str) -> (str, str):
        if (loop == ""):
            return (fixed, loop)
        k = min(len(fixed), len(loop))
        for i in range(0, k):
            #print((fixed[i - k:], loop[i - k:]))
            if (fixed[i - k:] == loop[i - k:]):
                
                loop = loop[i - k:] + loop[: i - k]
                fixed = fixed[: i - k]
                break
                
        loop = self.contract(loop)
        k = len(loop)
        while (len(fixed) >= k and fixed[-k: ] == loop):
            fixed = fixed[: -k]
        
        return (fixed, loop)
    def sj(self, integer, fixed, loop) -> (int, str, str):
        #print((integer, fixed, loop))
        if (loop == "9"):
            num = int(str(integer) + str(fixed))
            num += 1
            # num * 1e(-len(fixed))
            num = str(num)
            num = num.zfill(len(str(integer)) + len(fixed))
            integer = int(num[: len(num) - len(fixed)])
            fixed = num[len(num) - len(fixed): ]
            loop = ""
        if (loop == "0" or loop == ""):
            loop = ""
            if (fixed != "" and int(fixed) == 0):
                fixed = ""
        return (integer, fixed, loop)
    def isRationalEqual(self, S: str, T: str) -> bool:
        
        s = S.split(".")
        t = T.split(".")
        
        intS = int(s[0])
        floatS = s[1] if len(s) == 2 else ""
        intT = int(t[0])
        floatT = t[1] if len(t) == 2 else ""

        #print(floatT)
        (fixedS, loopS) = self.form(floatS)
        (fixedT, loopT) = self.form(floatT)
        #print((intT, fixedT, loopT))
        '''print(fixedS)
        print(loopS)
        print(fixedT)
        print(loopT)'''
        
        (fixedS, loopS) = self.minimium(fixedS, loopS)
        (fixedT, loopT) = self.minimium(fixedT, loopT)
        #print((intS, fixedS, loopS))
        #print((intT, fixedT, loopT))
        (intS, fixedS, loopS) = self.sj(intS, fixedS, loopS)
        print((intS, fixedS, loopS))
        (intT, fixedT, loopT) = self.sj(intT, fixedT, loopT)
        
        print((intT, fixedT, loopT))
        return (intS == intT and fixedS == fixedT and loopS == loopT)