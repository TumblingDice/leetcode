# https://leetcode-cn.com/problems/minimize-malware-spread/


from typing import List



class Solution:

    def __init__(self):
        self.n = 0

        self.father = []
        self.g = []


    def findFather(self, x):
        #print(len(self.father))
        if self.father[x] == x:
            return x
        self.father[x] = self.findFather(self.father[x])
        return self.father[x]

    

    def search(self, initial):
        
        q = [] 
        for i in range(0, self.n):
            if self.g[initial][i] == 1 and i != initial:
                q.append(i)
        while len(q) != 0:
            t = q.pop(0)
            if self.father[t] == -1:
                pass
                self.father[t] = self.findFather(initial)
                for i in range(0, self.n):
                    if self.g[t][i] == 1 and t != i:
                        q.append(i)

            else:
                pass
                self.father[self.findFather(t)] = self.father[self.findFather(initial)] = self.n



    def minMalwareSpread(self, graph: List[List[int]], initial: List[int]) -> int:
        
        self.n = len(graph)

        self.father = [-1] * (self.n + 1)

        self.g = graph

        for node in initial:
            self.father[node] = node
        self.father[self.n] = self.n
        for i in initial:
            self.search(i)

        count = [0] * (self.n + 1)
        ans_n = -1
        ans_i = self.n
        for i in range(0, self.n):
            f = self.findFather(i)
            if f >= 0:
                count[f] += 1
        
        for i in initial:
            if ans_n < count[i]:
                ans_n = count[i]
                ans_i = i

        return ans_i
            
'''
if __name__ == "__main__":
    s = Solution()
    ans = s.minMalwareSpread( [[1,0,0],[0,1,0],[0,0,1]],[0,2])

    print(ans)
    '''