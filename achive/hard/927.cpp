// https://leetcode-cn.com/problems/three-equal-parts/
class Solution 
{
public:
    vector<int> threeEqualParts(vector<int>& A) 
    {
        int n = A.size();
        int m = 0;
        vector<int> ans = {-1, -1};
        vector<int> b = {-1, -1, -1};
        for (auto i: A)
        {
            m += i;
        }

        if (m % 3 != 0)
        {
            return ans;
        }
        else if (m == 0)
        {
            if (n >= 3)
            {
                ans = {0, 2};
            }
            else
            {
                ans = {-1, -1};
            }
            return ans;
        }
        m = m / 3;
        //printf("m: %d, %d\n", m, 1 + m * 2);
        for (int i = 0, sum = 0; i < n; i++)
        {
            sum += A[i];
            if (A[i] == 1)
            {
                if (sum == 1)
                {
                    b[0] = i;
                }
                else if (sum == 1 + m)
                {
                    b[1] = i;
                }
                else if (sum == 1 + m * 2)
                {
                    b[2] = i;
                }
            }
            
        }
        //printf("n: %d, b2: %d\n", n, b[2]);
        int posLen = (n - b[2]);

       // printf("b0: %d, b1: %d, b2: %d, posLen: %d\n", b[0], b[1], b[2], posLen);
        if ((b[0] + posLen > b[1]) || (b[1] + posLen > b[2]))
        {
            ans = {-1, -1};
            return ans;
        }
        
        for (int i = 0; i < posLen; i++)
        {
            if ((A[b[0] + i] != A[b[2] + i]) || (A[b[1] + i] != A[b[2] + i]))
            {
                ans = {-1, -1};
                return ans;
            }
            
        }
        ans[0] = b[0] + posLen - 1;
        ans[1] = b[1] + posLen;

        return ans;

    }
};