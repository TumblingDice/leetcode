// https://leetcode-cn.com/problems/distinct-subsequences-ii/submissions/
class Solution 
{
public:
    static const int MOD = 1000000007;
    int distinctSubseqII(string S) 
    {
        map<char, int> last;
        long long dp[2006];
        memset(dp, -1, sizeof(dp));
        dp[0] = 0;
        for (int i = 0; i < S.size(); i++)
        {
            if (last.find(S.at(i)) == last.end())
            {
                dp[i + 1] = ((long long) dp[i] * 2 + 1) % MOD;
            }
            else 
            {
                dp[i + 1] = ((long long) dp[i] * 2 - dp[last[S.at(i)]]) % MOD;
            }
            // 为什么这里会负呢?
            // 因为mod
            if (dp[i + 1] < 0)
            {
                dp[i + 1] += MOD;
            }
            last[S.at(i)] = i;
        }


        return dp[S.size()];
    }
};