# https://leetcode-cn.com/problems/binary-tree-cameras/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:

    def __init__(self):
        self.ans = 0
    def minCameraCover(self, root: TreeNode) -> int:

        

        if root == None:
            return 0
        if root.left == None and root.right == None:
            return 1
        def search(t: TreeNode):
            
            if t == None:
                return [False, True]
            if t.left == None and t.right == None:
                return [False, False]
            

            
            
            [lSet, lSpv] = search(t.left)
            [rSet, rSpv] = search(t.right)

            if t == root:
                if (not lSet) and (not rSet):
                    self.ans += 1
                    return [True, True]
            if (not lSpv) or (not rSpv):
                self.ans += 1
                #print(label)
                return [True, True]
            else:
                return [False, (lSet or rSet)]
            
        search(root)

        return self.ans