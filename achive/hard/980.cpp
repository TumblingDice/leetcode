// https://leetcode-cn.com/problems/unique-paths-iii/
#include <stack>
#include <vector>
#include <iostream>
class Solution 
{
public:

    vector<vector<int> >* g;
    vector<vector<int> >* f;
    int h, w, sx, sy, ex, ey;
    int target = 0, count = 0;
    int ans = 0;
    int tx[4] = {-1, 0, 1, 0};
    int ty[4] = {0, 1, 0, -1};
    void dfs(int x, int y)
    {
       // cout << x << " " << y << endl;

        

        for (int i = 0; i < 4; i++)
        {
            int nx = x + tx[i];
            int ny = y + ty[i];
            if (nx >= 0 && ny >= 0 && nx < h && ny < w)
            {
                if ((*f)[nx][ny] == 0)
                {
                    if ((*g)[nx][ny] == 0)
                    {
                        (*f)[x][y] = 1;
                        count += 1;
                        dfs(nx, ny);
                        count -= 1;
                        (*f)[x][y] = 0;
                    }
                    else if ((*g)[nx][ny] == 2 && count == target)
                    {
                        ans += 1;
                        return;
                    }
                    
                }
            }
        }

        
    }



    int uniquePathsIII(vector<vector<int>>& grid) 
    {
        this->g = &grid;
        h = grid.size();
        w = grid[0].size();
        f = new vector<vector<int> > (grid);
        for (int x = 0; x < h; x++)
        {
            for (int y = 0; y < w; y++)
            {
                (*f)[x][y] = 0;
                if (grid[x][y] == 1)
                {
                    sx = x;
                    sy = y;
                }
                else if (grid[x][y] == 2)
                {
                    ex = x;
                    ey = y;
                }
                else if (grid[x][y] == 0)
                {
                    target += 1;
                }
            }
        }
        dfs(sx, sy);

        delete f;

        return ans;
    }
};