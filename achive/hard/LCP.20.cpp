// https://leetcode-cn.com/problems/meChtZ/
class Solution 
{
    
public:
    map<int, unsigned long long> dp;
    int inc, dec;
    unsigned long long ans;
    vector<int> jump, cost;
    unsigned long long search(int pos, unsigned long long sum)
    {
        //printf("pos: %d, sum: %lld, ans: %lld\n", pos, sum, ans);
        if (dp.find(pos) != dp.end())
        {
            return dp[pos];
        }
        
        unsigned long long ret = (unsigned long long) (inc) * (pos);
        if (sum < ans)
        {
            for (int i = 0; i < jump.size(); i++)
            {
                
                if (jump[i] <= 1)
                {
                    continue;
                }
                printf("%d, %d\n", pos, jump[i]);
                ret = min(ret, 
                        inc * (unsigned long long )(pos % jump[i]) + cost[i] +   search(pos / jump[i],  sum + inc * (unsigned long long )(pos % jump[i]) + cost[i]  )
                    );
                if (pos % jump[i] != 0)
                {
                    ret = min(ret, 
                            dec * (unsigned long long)(jump[i] - (pos % jump[i])) + cost[i] + 
                                search(pos / jump[i] + 1,  sum + dec * (unsigned long long)(jump[i] - (pos % jump[i])) + cost[i])
                            );
                }
            }
            dp[pos] = ret;
            //ans = min(ans, ret + sum);
        }
        
        return ret;
    }
    int busRapidTransit(int target, int inc, int dec, vector<int>& jump, vector<int>& cost) 
    {
        this->inc = inc;
        this->dec = dec;
        this->jump = jump;
        this->cost = cost;
        ans = (unsigned long long) (inc) * target;
        dp[0] = 0;
        dp[1] = inc;
        auto ret = search(target, 0) % (1000000007);

        printf("%lld", ans);
        return ret;
    }
};