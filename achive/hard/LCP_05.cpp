class Solution {
public:
    unsigned long long sum[50005], all[50005];
    int father[50005];
    int size[50005];
    vector<int> son[50005];
    const int MOD = 1000000007;

    int get_size(int u)
    {
        if (size[u] != 0)
        {
            return size[u];
        }
        int s = 1;
        for (auto& v: son[u])
        {
            s += get_size(v);
        }
        size[u] = s;
        return s;
    }
    vector<int> bonus(int n, vector<vector<int>>& leadership, vector<vector<int>>& operations) 
    {
        memset(sum, 0, sizeof(sum));
        memset(all, 0, sizeof(all));
        memset(father, 0, sizeof(father));
        memset(size, 0 , sizeof(size));
        vector<int> res;
        for (auto& pair: leadership)
        {
            auto& f = pair[0];
            auto& s = pair[1];
            father[s] = f;
            son[f].push_back(s);
        }
        for (auto& op: operations)
        {
            int u = op[1];
            vector<int> pred;
            while (u != 0)
            {
                pred.push_back(u);
                u = father[u];
            }
            /*printf("path: ");
            for (int i = pred.size() - 1; i >= 0; i--)
            {
                printf("%d ", pred[i]);
            }
            printf("\n");*/
            if (op[0] == 1)
            {

                for (int i = pred.size() - 1; i >= 0; i--)
                {
                    u = pred[i];
                    sum[u] = ((long long)(sum[u] + op[2])) % MOD;
                }
            }
            else if (op[0] == 2)
            {
                int s = ((long long)(get_size(op[1]) * op[2])) % MOD;
                //printf("%d: s = %d\n", op[1], s);
                for (int i = pred.size() - 1; i >= 0; i--)
                {
                    u = pred[i];
                    sum[u] = ((long long)(sum[u] + s)) % MOD;
                }
                for (auto& v: son[op[1]])
                {
                    all[v] = (long long)(all[v] + op[2]) % MOD;
                }
            }
            else if (op[0] == 3)
            {
                for (int i = pred.size() - 1; i >= 0; i--)
                {
                    u = pred[i];
                    if (all[u] != 0)
                    {
                        sum[u] = (long long) (sum[u] + (all[u] % MOD) * (get_size(u) % MOD)) % MOD;
                        all[u] = 0;
                        for (auto& v: son[u])
                        {
                            all[v] = ((long long) (all[v] + all[u])) % MOD;
                        }
                    }
                }
                
                res.push_back((int) (sum[op[1]] % MOD));
            }
        }

        return res;
    }
};