# https://leetcode-cn.com/problems/shu-zu-zhong-de-ni-xu-dui-lcof/


int buf[50001];
int *a;
int ans;
void sort(int l, int r)
{
    if (l == r)
    {
        return;
    }
    int mid = (l + r) / 2;
    sort(l, mid);
    sort(mid + 1, r);
    int i = l, j = mid + 1;
    int c = l;
    while (i <= mid && j <= r)
    {
        if (a[i] <= a[j])
        {
            buf[c] = a[i];
            i++;
        }
        else
        {
            buf[c] = a[j];
            j++;

            ans += (mid - i + 1);
        }
        c++;
    }
    while (i <= mid)
    {
        buf[c] = a[i];
        i++;
        c++;

    }
    while (j <= r)
    {
        buf[c] = a[j];
        j++;
        c++;
    }

    for (int k = l; k <= r; k++)
    {
        a[k] = buf[k];
    }
    return; 
}




int reversePairs(int* nums, int numsSize)
{
    if (numsSize == 0)
    {
        return 0;
    }
    a = nums;
    ans = 0;
    sort(0, numsSize - 1);


    return ans;


}