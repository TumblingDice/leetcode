// https://leetcode-cn.com/problems/closed-number-lcci/


class Solution 
{
public:


    void makeMax(int& num, int l)
    {
        int c = 0;
        for (int i = 0; i <= l; i++)
        {
            if ((num & (1 << i)) != 0)
            {
                c += 1;
            }
        }
        for (int i = 0; i < c; i++)
        {
            num = num | (1 << (l - i));
        }

        for (int i = l - c; i >= 0; i--)
        {
            num = num & (~(1 << i));
        }
        //num = num & (-1 << (l - c + 1));
    }


    void makeMin(int& num, int l)
    {
        if (l < 0)
        {
            return;
        }
        int c = 0;
        for (int i = 0; i <= l; i++)
        {
            if ((num & (1 << i)) == 0)
            {
                c += 1;
            }
        }
        for (int i = 0; i < c; i++)
        {
            num = num & (~(1 << (l - i)));
        }

        for (int i = l - c; i >= 0; i--)
        {
            num = num | (1 << i);
        }
        //num = num | (~(-1 << (l - c + 1)));
    }
    int leastSig(int num)
    {
        for (int i = 0; i < 32; i++)
        {
            if ((num & (1 << i)) != 0)
            {
                return i;
            }
        }
        return -1;
    }

    int mostSig(int num)
    {
        for (int i = 31; i >= 0; i--)
        {
            if ((num & (1 << i)) != 0)
            {
                return i;
            }
        }
        return -1;
    }
    
    int setBit(int num, int pos, int b)
    {
        int ret;
        if (b == 0)
        {
            ret = num & (~(1 << pos));
        }
        else if (b == 1)
        {
            ret = num | (1 << pos);
        }
        return ret;
    }

    pair<int, int> gapZero(int num)
    {
        pair<int, int> ret;
        ret.first = ret.second = -1;
        bool ok = false;
        int i;
        for (i = leastSig(num) + 1; i < 32; i++)
        {
            if ((num & (1 << i)) == 0)
            {
                ok = true;
                break;
            }
        }
        
        if (ok)
        {
            ret.first = i;
            for (int j = i; j < 32; j++)
            {
                if ((num & (1 << j)) != 0)
                {
                    break;
                }
                ret.second = j;
            }
        }
        return ret;
    }


    vector<int> findClosedNumbers(int num) 
    {
        vector<int> ans = {-1, -1};
        if (leastSig(num) == 0)
        {
            if (gapZero(num).first == -1 || gapZero(num).first > mostSig(num))
            {
                ans[1] = -1;
            }
            else
            {
                ans[1] = setBit(num, gapZero(num).second + 1, 0);
                ans[1] = setBit(ans[1], gapZero(num).second, 1);

                makeMax(ans[1], gapZero(num).second - 1);
                /*
                int i;
                for (i = 0; i < (gapZero(num).first - leastSig(num)); i++)
                {
                    ans[1] = setBit(ans[1], gapZero(num).second - i - 1, 1);
                }
                for (i = gapZero(num).second - i - 1; i >= 0; i--)
                {
                    ans[1] = setBit(ans[1], i, 0);
                }*/
            }
        }
        else
        {
            ans[1] = (num ^ (1 << leastSig(num))) | (1 << (leastSig(num) - 1));
        }


        if (gapZero(num).first == -1)
        {
            ans[0] = -1;
        }
        else
        {
            ans[0] = setBit(num, gapZero(num).first, 1);
            ans[0] = setBit(ans[0], gapZero(num).first - 1, 0);
            makeMin(ans[0], gapZero(num).first - 2);
        }

        return ans;
    }
};