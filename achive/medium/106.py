# https://leetcode-cn.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        

        def search(l: int, r: int):
            

            #print("L: %d, R: %d" % (l, r))
            if l > r:
                return None
            root = TreeNode()
            root.val = postorder.pop(-1)
            root.left = None
            root.right = None
            
            
            mid = inorder.index(root.val)
            #print("mid: %d" % (mid))
            root.right = search(mid + 1, r)

            root.left = search(l, mid - 1)
            

            return root


        return search(0, len(inorder) - 1)