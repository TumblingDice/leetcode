# https://leetcode-cn.com/problems/path-sum-ii/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import copy
class Solution:

    def __init__(self):
        self.ans = []
        self.buf = []
        self.s = 0
    def pathSum(self, root: TreeNode, sum: int) -> List[List[int]]:
        
        if root == None:
            return []
        
        def dfs(t: TreeNode):
            self.buf.append(t.val)
            self.s += t.val
            if t.left == None and t.right == None and self.s == sum:
                self.ans.append(copy.deepcopy(self.buf))

            else:
                if t.left != None:
                    dfs(t.left)
                if t.right != None:
                    dfs(t.right)

            self.s -= t.val
            self.buf.pop(-1)

        dfs(root)
        return self.ans