// https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node-ii/p


/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution 
{
public:
    Node* connect(Node* root) 
    {
        if (root == nullptr)
        {
            return root;
        }
        queue<Node*> q[2];
        q[0].push(root);
        int idx = 0;
        Node *r = nullptr;
        while (!q[idx].empty())
        {
            r = nullptr;
            int next_idx = (idx + 1) % 2;
            while (!q[idx].empty())
            {
                auto t = q[idx].front();
                q[idx].pop();
                t->next = r;
                r = t;
                if (t->right != nullptr)
                {
                    q[next_idx].push(t->right);
                }
                if (t->left != nullptr)
                {
                    q[next_idx].push(t->left);
                }
            }
            idx = next_idx;
        }
        return root;
    }

    
};