// https://leetcode-cn.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution 
{
public:
    TreeNode *original, *cloned, *target, *ans;

    int dfs(TreeNode* cur_original, TreeNode* cur_cloned)
    {
        if (cur_original == target)
        {
            ans = cur_cloned;
            return 0;
        }

        if (cur_original->left != nullptr)
        {
            int res = dfs(cur_original->left, cur_cloned->left);
            if (res == 0)
            {
                return 0;
            }
        }
        if (cur_original->right != nullptr)
        {
            int res = dfs(cur_original->right, cur_cloned->right);
            if (res == 0)
            {
                return 0;
            }
        }
        return 1;
    }


    TreeNode* getTargetCopy(TreeNode* original, TreeNode* cloned, TreeNode* target) 
    {
        this->original = original;
        this->cloned = cloned;
        this->target = target;
        
        dfs(original, cloned);
        return ans;
    }
};