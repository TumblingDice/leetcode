# https://leetcode-cn.com/problems/binary-tree-postorder-traversal/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def __init__(self):
        self.ans = []
    def postorderTraversal(self, root: TreeNode) -> List[int]:
        
        def dfs(t: TreeNode):
            
            if t == None:
                return
            
            dfs(t.left)
            dfs(t.right)
            self.ans.append(t.val)
            return
        dfs(root)
        return self.ans
        