# https://leetcode-cn.com/problems/course-schedule-iv/
class Solution:
    def __init__(self):
        self.pre = []
        self.edge = []
        self.visit = []
    def checkIfPrerequisite(self, n: int, prerequisites: List[List[int]], queries: List[List[int]]) -> List[bool]:
        self.pre = [set()] * n
        self.visit = [0] * n
        for i in range(0, n):
            self.edge.append([])

        for edge in prerequisites:
            self.edge[edge[1]].append(edge[0])
            
        def dfs(t: int):
            if self.visit[t] != 0:
                return
            self.visit[t] = 1
            s = set()
            s.add(t)
            for next in self.edge[t]:
                dfs(next)
                s = (s | self.pre[next])
            self.pre[t] = s
            return
            
        ans = []
        #dfs(1)
        for q in queries:
            dfs(q[1])
            #print("%d req: %s" % (q[1], str(self.pre[q[1]])))
            ans.append((q[0] in self.pre[q[1]]))
        #print("%d req: %s" % (1, str(self.pre[1])))
        return ans