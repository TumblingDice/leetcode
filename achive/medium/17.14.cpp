// https://leetcode-cn.com/problems/smallest-k-lcci/
class Solution {
public:
    int sort(vector<int>& a, int l, int r)
    {
        int t = a[l];
        while (l < r)
        {
            while (l < r && a[r] >= t)
            {
                r--;
            }
            a[l] = a[r];
            while (l < r && a[l] <= t)
            {
                l++;
            }
            a[r] = a[l];
        }
        a[l] = t;
        return l;
    }
    vector<int> smallestK(vector<int>& arr, int k) {

        int n = arr.size();
        vector<int> ans;
        if (n == 0)
        {
            return ans;
        }
        vector<int>& a = arr;
        
        

        int l = 0, r = n - 1;
        

        while (true)
        {
            int mid = sort(a, l, r);
            //printf("\n");
            if (mid == k)
            {
                for (int i = 0; i < k; i++)
                {
                    ans.push_back(a[i]);
                }
                return ans;
            }
            else if (mid < k)
            {
                l = mid + 1;
            }
            else 
            {
                r = mid - 1;
            }

        }
    }
};