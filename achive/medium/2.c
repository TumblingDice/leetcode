// https://leetcode-cn.com/problems/add-two-numbers/
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2)
{
    struct ListNode *res = malloc(sizeof(struct ListNode));
    res->val = 0;
    res->next = 0;
    struct ListNode *cur1 = l1, *cur2 = l2, *cur3 = res;
    
    int f = 0;
    while (cur1 != 0 && cur2 != 0)
    {
        
        cur3->val = cur1->val + cur2->val + cur3->val;
        //f = 0;
        if (cur3->val > 9)
        {
            //printf("a");
            cur3->val -= 10;
            cur3->next = (struct ListNode *) malloc(sizeof(struct ListNode));
            cur3->next->next = 0;
            cur3->next->val = 1;
        }
        else
        {
           // printf("b");
            if (cur1->next != 0 || cur2->next != 0)
            {
                //printf("?");
                cur3->next = malloc(sizeof(struct ListNode));
                cur3->next->next = 0;
                cur3->next->val = 0;
            }
        }
        
        cur1 = cur1->next;
        cur2 = cur2->next;
        cur3 = cur3->next;
    }
    while (cur1 != 0)
    {
        cur3->val = cur3->val + cur1->val;
        if (cur3->val > 9)
        {
            cur3->val -= 10;
            cur3->next = (struct ListNode *) malloc(sizeof(struct ListNode));
            cur3->next->next = 0;
            cur3->next->val = 1;
        }
        else
        {
            if (cur1->next != 0)
            {
                cur3->next = malloc(sizeof(struct ListNode));
                cur3->next->next = 0;
                cur3->next->val = 0;
            }
        }
        cur1 = cur1->next;
        cur3 = cur3->next;
    }
    while (cur2 != 0)
    {
        cur3->val = cur3->val + cur2->val;
        if (cur3->val > 9)
        {
            cur3->val -= 10;
            cur3->next = (struct ListNode *) malloc(sizeof(struct ListNode));
            cur3->next->next = 0;
            cur3->next->val = 1;
        }
        else
        {
            if (cur2->next != 0)
            {
                cur3->next = malloc(sizeof(struct ListNode));
                cur3->next->next = 0;
                cur3->next->val = 0;
            }
        }
        cur2 = cur2->next;
        cur3 = cur3->next;
    }

    return res;
}