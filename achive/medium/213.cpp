class Solution 
{
public:
    int solve(vector<int>& nums, int l, int r)
    {
        int n = r - l + 1;
        if (l >= r)
        {
            return 0;
        }
        if (n == 1)
        {
            return nums[l];
        }
        if (n == 2)
        {
            return max(nums[l], nums[l + 1]);
        }
        int dp[500];
        memset(dp, -1, sizeof(dp));
        dp[l] = nums[l];
        dp[l + 1] = nums[l + 1];
        for (int i = l; i < r - 1; i++)
        {
            dp[i + 1] = max(dp[i + 1], dp[i]);
            dp[i + 2] = max(dp[i + 2], dp[i] + nums[i + 2]);
        }
        dp[r - 1] = max(dp[r - 1], dp[r - 2]);
        dp[r] = max(dp[r], dp[r - 1]);
        dp[r] = max(dp[r], dp[r - 2] + nums[r]);

        return dp[r];
    }
    int rob(vector<int>& nums) 
    {
        /***
        dp[i] = max(nums[i] + dp[i - 2], dp[i - 1])
        dp[i] updates dp[i + 1] and dp[i + 2]
        ***/
        if (nums.size() == 1)
        {
            return nums[0];
        }
        return max(solve(nums, 1, nums.size() - 1), solve(nums, 0, nums.size() - 2));
    }
};