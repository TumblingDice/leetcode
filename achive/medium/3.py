https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        ans = 0
        l = 0
        dic = {}
        for p in range(0, len(s)):
            if s[p] not in dic:
                pass
            else:
                for i in range(l, dic[s[p]]):
                    dic.pop(s[i])
                l = dic[s[p]] + 1
            dic[s[p]] = p
            ans = max(ans, (p - l) + 1)

        #print(ans)
        return ans