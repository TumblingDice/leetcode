# https://leetcode-cn.com/problems/top-k-frequent-elements/

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        dic = {}
        for num in nums:
            if num in dic:
                dic[num] += 1
            else:
                dic[num] = 1
        #print (dic)
        res = sorted(dic.items(), key = lambda x: x[1], reverse = True)

        #print (res)
        return [x[0] for x in res[:k]]