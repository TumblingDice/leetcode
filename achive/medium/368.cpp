// https://leetcode-cn.com/problems/largest-divisible-subset/
class Solution 
{
public:

    vector<int> largestDivisibleSubset(vector<int>& nums) 
    {
        int n = nums.size();
        if (n == 0)
        {
            return vector<int> ();
        }
        sort(nums.begin(), nums.end());
        int max_i;
        int max_c = -1;
        vector<int> dp (nums);
        vector<int> pre (nums);
        for (int i = 0; i < n; i++)
        {
            dp[i] = 0;
            pre[i] = -1;
            for (int j = i - 1; j >= 0; j--)
            {
                if (nums[i] % nums[j] == 0)
                {
                    if (dp[i] < dp[j] + 1)
                    {
                        dp[i] = dp[j] + 1;
                        pre[i] = j;
                    }
                    
                }
            }

            if (max_c < dp[i])
            {
                max_c = dp[i];
                max_i = i;
            }
        }


        vector<int> ans;
        if (max_i == -1)
        {
            
            return ans;
        }
        int cur = max_i;
        while (true)
        {
            ans.push_back(nums[cur]);
            cur = pre[cur];
            if (cur == -1)
            {
                break;
            }
        }
        return ans;
    }
};