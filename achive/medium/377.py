# https://leetcode-cn.com/problems/combination-sum-iv/
class Solution:
    def __init__(self):
        self.cand = []
        self.sol = {}
    def search(self, target: int) -> int:
        if target in self.sol:
            return self.sol[target]

        

        if target == 0:
            return 1
        if min(self.cand) > target:
            return 0
        
        ret = 0
        for c in self.cand:
            ret += self.search(target - c)
        self.sol[target] = ret
        return ret

    def combinationSum4(self, nums: List[int], target: int) -> int:
        if len(nums) == 0:
            return 0
        self.cand = sorted(nums, key = lambda x: x)
        #print(self.cand)
        ans = self.search(target)
        #print(self.sol)
        return ans