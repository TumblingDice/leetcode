# https://leetcode-cn.com/problems/longest-absolute-file-path/

lastDepth = -1
curLength = -1
maxLength = -1
curPath = []
maxPath = []

a = input()
a = a[1:-1]
s = a.split("\\n")
for line in s:
    curDepth = 0
    while len(line) >= 2 and line[0:2] == "\\t":
        curDepth += 1
        line = line[2:]

    #line = line[curDepth:]
    #print(str(curDepth) + ", " + line)
    if curDepth == lastDepth + 1:
        curPath.append(line)

    elif curDepth <= lastDepth:
        #print(curPath)
        curPath = curPath[:-(lastDepth - curDepth + 1)]
        #print(curPath)
        curPath.append(line)
        #print(curPath)
    lastDepth = curDepth
    if line.find(".") != -1 and len("/".join(maxPath)) < len("/".join(curPath)):
        maxPath = curPath

#print(maxPath)
print(len("/".join(maxPath)))