# https://leetcode-cn.com/problems/combination-sum/
class Solution:

    def __init__(self):
        self.sol = {}
        self.cand = []
        self.res = []
        self.ans = []
    def search(self, target: int, depth: int):
        
        if depth == len(self.cand):
            return
        if target < self.cand[depth]:
            return
        s = 0
        
        tmp = len(self.res)

        while s < target:
            
            self.search(target - s, depth + 1)
            self.res.append(self.cand[depth])
            s += self.cand[depth]
        if s == target:
            #print(self.res)
            self.ans.append(self.res)
            
        self.res = self.res[:tmp]
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        self.cand = sorted(candidates, key = lambda x: x)
        #print(self.cand)
        self.search(target, 0)

        print(self.ans)
        return self.ans
