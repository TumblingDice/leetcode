# https://leetcode-cn.com/problems/combination-sum-ii/
class Solution:
    def __init__(self):
        self.buf = []
        self.ans = []
        self.cand = []
        self.dic = {}
    def search(self, depth: int, target: int):
        print("depth: %d, target: %d" % (depth, target))
        if target == 0:
            print(self.buf)
            self.ans.append(self.buf.copy())
            print(self.ans)
            return
        if depth == len(self.cand):
            return
        if self.cand[depth] > target:
            return
        
        self.search(depth + 1, target)
        n = self.dic[self.cand[depth]]
        count = 0
        for i in range(0, n):
            if target < (i + 1) * self.cand[depth]:
                break
            self.buf.append(self.cand[depth])
            count += 1
            self.search(depth + 1, target - (i + 1) * self.cand[depth])
        
        self.buf = self.buf[:(-count)]

    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        
        for c in candidates:
            if c in self.dic:
                self.dic[c] += 1
            else:
                self.dic[c] = 1
        self.cand = sorted(self.dic.keys(), key = lambda x: x)
        #self.cand = sorted(candidates, key = lambda x: x)
        print(self.cand)
        self.search(0, target)
        return self.ans