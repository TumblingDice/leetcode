# https://leetcode-cn.com/problems/path-sum-iii/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

import copy
class Solution:


    def __init__(self):
        self.path = []
        self.ans = 0
        self.s = 0
    def pathSum(self, root: TreeNode, sum: int) -> int:

        def testSum():
            

            tmp = self.path[-1]
            for i in range(len(self.path) - 2, -1, -1):
                if tmp == sum:
                    #self.ans.append(copy.deepcopy(self.path[i:]))
                    self.ans += 1
                tmp += self.path[i]


        def search(t: TreeNode):
            
            if t == None:
                return
            
            self.path.append(t.val)
            testSum()   # 应该再维护个数据结构处理后缀和
            

            search(t.left)
            search(t.right)

            self.path.pop(-1)
            return

        
        search(root)

        return self.ans