
# https://leetcode-cn.com/problems/permutations/
import copy

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:

        ans = []
        buf = []
        cand = {}

        for num in nums:
            if num not in cand:
                cand[num] = 1
            else:
                cand[num] += 1
        cand = [[k, v] for [k, v] in zip(cand.keys(), cand.values())]

        def search(count: int, last: int):


            #print("count: %d, last: %d" % (count, last))
            #print(buf)
            if count == len(nums):
                ans.append(copy.deepcopy(buf))
                return
            
            for i in range(len(cand)):
                if i == last:
                    continue
                tmp = cand[i][1]
                for j in range(tmp):
                    
                    cand[i][1] -= 1
                    buf.append(cand[i][0])
                    
                    search(count + j + 1, i)

                cand[i][1] = tmp
                for j in range(tmp):
                    buf.pop(-1)
        
        search(0, -1)

        return ans        