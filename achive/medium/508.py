# https://leetcode-cn.com/problems/most-frequent-subtree-sum/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:

    def __init__(self):
        self.dic = {}

    def dfs(self, t: TreeNode) -> int:

        sum = t.val
        if t.left != None:
            sum += self.dfs(t.left)
        if t.right != None:
            sum += self.dfs(t.right)
        
        if sum in self.dic:
            self.dic[sum] += 1
        else:
            self.dic[sum] = 1
        
        return sum
    def findFrequentTreeSum(self, root: TreeNode) -> List[int]:

        if root == None:
            return []
        self.dfs(root)
        
        ans = []
        ans_v = max(self.dic.values())
        
        for [k, v] in self.dic.items():
            if v == ans_v:
                ans.append(k)
        
        return ans