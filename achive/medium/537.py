# https://leetcode-cn.com/problems/complex-number-multiplication/submissions/
class Solution:
    def complexNumberMultiply(self, a: str, b: str) -> str:
        tmp = a.split("+")
        a1 = int(tmp[0])
        a2 = int((tmp[1])[: -1])
        tmp = b.split("+")
        
        b1 = int(tmp[0])
        b2 = int((tmp[1])[: -1])
        

        c1 = (a1 * b1) - (a2 * b2)
        c2 = (a1 * b2) + (a2 * b1)

        res = str(c1) + "+" + str(c2) + "i"

        return res