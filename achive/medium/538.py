# https://leetcode-cn.com/problems/convert-bst-to-greater-tree/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def convertBST(self, root: TreeNode) -> TreeNode:
        if root == None:
            return None
        a = []
        def dfs(t: TreeNode) -> int:

            if t.right != None:
                dfs(t.right)
            
            a.append(t)
            
            if t.left != None:
                dfs(t.left)
            
        
        dfs(root)
        s = 0
        for t in a:
            tmp = t.val
            t.val += s
            s += tmp
        return root