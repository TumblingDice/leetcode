// https://leetcode-cn.com/problems/number-of-provinces/




class Solution {
public:
    int n;
    int visit[205];
    vector<vector<int>> *G;
    void dfs(int u)
    {
        if (visit[u] != -1)
        {
            return;
        }
        visit[u] = 1;
        for (int v = 0; v < n; v++)
        {
            if ((*G)[u][v] == 1)
            {
                dfs(v);
            }
        }
    }
    int findCircleNum(vector<vector<int>>& isConnected) 
    {
        G = &isConnected;
        int ans = 0;
        n = isConnected.size();
        memset(visit, -1, sizeof(visit));
        for (int i = 0; i < n; i++)
        {
            if (visit[i] == -1)
            {
                ans += 1;
                dfs(i);
            }
        }


        return ans;
    }
};