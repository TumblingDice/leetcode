// https://leetcode-cn.com/problems/design-circular-deque/
class MyCircularDeque {
public:

    int *element;
    int size;
    int capacity;
    int front, back;
    /** Initialize your data structure here. Set the size of the deque to be k. */
    MyCircularDeque(int k) {
        element = new int [k];
        capacity = k;
        size = 0;

        front = back = 0;
        
    }
    
    
    /** Adds an item at the front of Deque. Return true if the operation is successful. */
    bool insertFront(int value) {
        if (size == capacity)
        {
            return false;
        }

        size += 1;
        front = (front == 0) ? (capacity - 1) : (front - 1);
        element[front] = value;


        return true;
    }
    
    /** Adds an item at the rear of Deque. Return true if the operation is successful. */
    bool insertLast(int value) 
    {
        if (size == capacity)
        {
            return false;
        }

        size += 1;
        element[back] = value;
        back = (back == capacity - 1) ? (0) : (back + 1);
        return true;
    }
    
    /** Deletes an item from the front of Deque. Return true if the operation is successful. */
    bool deleteFront() 
    {
        if (size == 0)
        {
            return false; 
        }
        size -= 1;
        front = (front == capacity - 1) ? (0) : (front + 1);
        return true;
    }
    
    /** Deletes an item from the rear of Deque. Return true if the operation is successful. */
    bool deleteLast() {
        if (size == 0)
        {
            return false; 
        }
        size -= 1;
        back = (back == 0) ? (capacity - 1) : (back - 1);
        return true;
    }
    
    /** Get the front item from the deque. */
    int getFront() {
        if (size == 0)
        {
            return -1;
        }
        return element[front];
    }
    
    /** Get the last item from the deque. */
    int getRear() {
        if (size == 0)
        {
            return -1;
        }
        return element[(back == 0) ? (capacity - 1) : (back - 1)];
    }
    
    /** Checks whether the circular deque is empty or not. */
    bool isEmpty() {
        return (size == 0);
    }
    
    /** Checks whether the circular deque is full or not. */
    bool isFull() {
        return (size == capacity);
    }
};