# https://leetcode-cn.com/problems/redundant-connection/
class Solution:
    def findRedundantConnection(self, edges: List[List[int]]) -> List[int]:
        

        father = [x for x in range(1005)]

        def findFather(x: int) -> int:
            if father[x] == x:
                return x
            father[x] = findFather(father[x])
            return father[x]

        def union(x: int, y: int) -> None:
            father[findFather(x)] = findFather(y)
        
        ans = [-1, -1]
        for [u, v] in edges:
            fu = findFather(u)
            fv = findFather(v)
            if fu == fv:
                ans = [u, v]
            else:
                union(fu, fv)

        return ans
        

