# https://leetcode-cn.com/problems/insert-into-a-binary-search-tree/
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def insertIntoBST(self, root: TreeNode, val: int) -> TreeNode:


        def insert(t: TreeNode, v: int):
            
            if v > t.val:
                if t.right != None:
                    insert(t.right, v)
                else:
                    t.right = TreeNode(v)
            else:
                if t.left != None:
                    insert(t.left, v)
                else:
                    t.left = TreeNode(v)
            
            return
        
        if root == None:
            root = TreeNode(val)
        else:
            insert(root, val)
        
        return root