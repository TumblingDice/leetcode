// https://leetcode-cn.com/problems/search-a-2d-matrix/

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) 
    {
        if (matrix.size() == 0)
        {
            return false;
        }
        if (matrix[0].size() == 0)
        {
            return false;
        }
        int h = matrix.size();
        int w = matrix[0].size();
        int n = h * w;
        int l = 0;
        int r = n;

        while (l < r - 1)
        {
            int mid = (l + r) / 2;
            
            int x = mid / w;
            int y = mid % w;

            if (matrix[x][y] == target)
            {
                return true;
            }
            else if (matrix[x][y] > target)
            {
                r = mid;
            }
            else 
            {
                l = mid;
            }
        }

        int x = l / w;
        int y = l % w;
        return matrix[x][y] == target;
    }
};