# https://leetcode-cn.com/problems/combinations/
class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        
        buf = []
        ans = []
        def search(depth, m):
            #print("%d %d" % (depth, m))
            if k == m:
                ans.append(buf.copy())
                return
            if depth == n:
                return
            

            buf.append(depth + 1)
            search(depth + 1, m + 1)
            buf.pop()

            search(depth + 1, m)
            
        search(0, 0)

        return ans