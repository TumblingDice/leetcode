# https://leetcode-cn.com/problems/subsets/
import copy
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        buf = []
        ans = []
        n = len(nums)
        def dfs(depth: int):
            if depth == n:
                ans.append(copy.deepcopy(buf))
                return
            dfs(depth + 1)
            
            buf.append(nums[depth])
            dfs(depth + 1)
            buf.pop(-1)
        
        dfs(0)
        return ans