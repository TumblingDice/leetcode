# https://leetcode-cn.com/problems/word-search/
class Solution:

    def __init__(self):
        self.grid = []
        self.target = ""
        self.visited = []
    def search(self, x: int, y: int, depth: int) -> bool:
        
        #print("entering (%d, %d), depth: %d" % (x, y, depth))
        self.visited[x][y] = True
        #print(self.visited)
        if depth == len(self.target) - 1:
            return True
        if (x > 0) and (y < len(self.grid[x - 1])) and (self.grid[x - 1][y] == self.target[depth + 1]) and (not self.visited[x - 1][y]):
            res = self.search(x - 1, y, depth + 1)
            if res == True:
                return True
        if (x + 1 < len(self.grid)) and (y < len(self.grid[x + 1])) and (self.grid[x + 1][y] == self.target[depth + 1]) and (not self.visited[x + 1][y]):
            res = self.search(x + 1, y, depth + 1)
            if res == True:
                return True

        if (y > 0) and (self.grid[x][y - 1] == self.target[depth + 1]) and (not self.visited[x][y - 1]):
            res = self.search(x, y - 1, depth + 1)
            if res == True:
                return True
        
        
        
        if (y + 1 < len(self.grid[x])) and (self.grid[x][y + 1] == self.target[depth + 1]) and (not self.visited[x][y + 1]):
            res = self.search(x, y + 1, depth + 1)
            if res == True:
                return True

        self.visited[x][y] = False
        return False


    def exist(self, board: List[List[str]], word: str) -> bool:
        self.grid = board
        self.target = word
        
        for x in range(0, len(board)):
            self.visited.append([False] * len(board[x]))
        
        for x in range(0, len(board)):
            for y in range(0, len(board[x])):

                if (board[x][y] == word[0]):
                    res = self.search(x, y, 0)
                    if res == True:
                        return True

        return False