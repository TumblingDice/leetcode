# https://leetcode-cn.com/problems/bitwise-ors-of-subarrays/
class Solution:
    def subarrayBitwiseORs(self, A: List[int]) -> int:
        
        n = len(A)
        res = set()
        cur = set()
        
        for i in range(n):
            
            next = set()
            for num in cur:
                next.add(num | A[i])
                res.add(num | A[i])
            next.add(A[i])
            res.add(A[i])
            cur = next
        
        return len(res)