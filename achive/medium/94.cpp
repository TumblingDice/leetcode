# https://leetcode-cn.com/problems/binary-tree-inorder-traversal/


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

#include <stack>
class Solution 
{
public:
    vector<int> inorderTraversal(TreeNode* root) 
    {

        stack<TreeNode*> s;
        vector<int> ans;
        if (root == nullptr)
        {
            return ans;
        }
        bool finish = false;
        s.push(root);
        while (!s.empty())
        {
            auto t = s.top();
            while ((t->left) != nullptr && (!finish))
            {
                s.push(t->left);
                t = t->left;
            }
            t = s.top();
            ans.push_back(t->val);
            s.pop();
            if (t->right != nullptr)
            {
                finish = false;
                s.push(t->right);
            }
            else
            {
                finish = true;
            }
            
        }

        return ans;
    }
};