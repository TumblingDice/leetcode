// https://leetcode-cn.com/problems/unique-binary-search-trees/submissions/
class Solution 
{
public:

    int numTrees(int n) 
    {
        long long dp[1232];
        dp[0] = 1;
        for (int i = 0; i <= n; i++)
        {
            dp[i + 1] = (dp[i] * (4 * i + 2)) / (i + 2);
        }

        return dp[n];
    }
};