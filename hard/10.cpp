// https://leetcode-cn.com/problems/regular-expression-matching/
class Solution {
public:
    struct Node
    {
        static int counter;
        int id;
        map<char, Node*> edge;
        bool isTerminal;
        Node(bool isTerminal)
        {
            this->isTerminal = isTerminal;
            id = ++counter;
        }
    };
    
    Node *start;
    void build(string& pattern)
    {
        start = new Node(false);
        Node *cur = start;
        
        for (int i = 0; i < pattern.length(); i++)
        {
            char &c = pattern.at(i);
            Node *next = new Node(false);
            
            if (i < pattern.length() - 1 && pattern.at(i + 1) == '*')
            {
                cur->edge['@'] = next;
                cur->edge[c] = cur;
                i++;
            }
            else 
            {
                cur->edge[c] = next;
            }
            cur = next;
        }
        cur->isTerminal = true;
    }
    bool check(string& s)
    {
        auto *curQueue = new queue<Node *>();
        auto *nextQueue = new queue<Node *> ();
        curQueue->push(start);
        for (int i = 0; i < s.length(); i++)
        {
            
            char &c = s.at(i);
            //printf("index: %d, char: %c\n", i, c);
            if (curQueue->empty())
            {
                //printf("curQueue empty\n");
                return false;
            }

            while (!curQueue->empty())
            {
                Node *curNode = curQueue->front();
                curQueue->pop();
                //printf("at node#%d\n", curNode->id);
                if (curNode->edge.find('@') != curNode->edge.end())
                {
                    //printf("found epsilon edge to node#%d\n", curNode->edge['@']->id);
                    curQueue->push(curNode->edge['@']);
                }
                if (curNode->edge.find('.') != curNode->edge.end())
                {
                    //printf("found . edge to node#%d\n", curNode->edge['.']->id);
                    nextQueue->push(curNode->edge['.']);
                }
                if (curNode->edge.find(c) != curNode->edge.end())
                {
                    //printf("found %c edge to node#%d\n", c, curNode->edge[c]->id);
                    nextQueue->push(curNode->edge[c]);
                }
            }
            swap(nextQueue, curQueue);
        }
        while (!curQueue->empty())
        {
            
            Node *curNode = curQueue->front();
            curQueue->pop();
            //printf("checking node#%d\n", curNode->id);
            if (curNode->isTerminal)
            {
                //printf("terminal");
                return true;
            }
            if (curNode->edge.find('@') != curNode->edge.end())
            {
                //printf("found epsilon edge to node#%d\n", curNode->edge['@']->id);
                curQueue->push(curNode->edge['@']);
            }
        }
        return false;
    }
    bool isMatch(string s, string p)
    {
        build(p);
        bool ans = check(s);

        return ans;

    }
};
int Solution::Node::counter = 0;