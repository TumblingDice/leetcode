// https://leetcode-cn.com/problems/maximum-number-of-darts-inside-of-a-circular-dartboard/
class Solution {
public:

    struct myCmp
    {
        bool doubleEqual(double x, double y)
        {
            return (x == y);
        }
        bool operator () (const pair<double, int>& x, const pair<double, int>& y)
        {
            if (doubleEqual(x.first, y.first))
            {
                return x.second > y.second;
            }
            return (x.first < y.first);
        }
    };

    

    void getWindow(vector<pair<double, int>>& angles, int x, int y, int r)
    {
        //printf("(%d, %d)\n", x, y);
        double m = dist(x, y, 0, 0);
        if (m > (2 * r))
        {
            return;
        }
        double theta = atan2(y, x);
        
        // atan2返回的是两种旋转方向的角度, 要映射到一个方向的[0, 2 * pi]
        theta = (theta < 0) ? (2 * M_PI + theta) : theta;
        
        
        double delta = acos(m / (2 * r));
        
        
        /*printf("theta = %f (%f pi)\tdelta = %f (%f pi)\n", 
                theta, theta / M_PI, delta, delta / M_PI);*/
        
        
        double start = theta - delta;
        double end = theta + delta;
        //printf("start: %f (%f pi)\tend: %f (%f pi)\n", start, start / M_PI, end, end / M_PI);
        if (start < 0)
        {
            start = 2 * M_PI + start;

            //printf("cross start = %f (%f pi)\n", start, start / M_PI);
            angles.push_back(pair<double, int> (start, 1));
            angles.push_back(pair<double, int> (0, 1));
            angles.push_back(pair<double, int> (end, -1));
        }
        else if (end >= (2 * M_PI))
        {
            end = end - (2 * M_PI);
            //printf("cross end = %f (%f pi)\n", end, end / M_PI);
            angles.push_back(pair<double, int> (start, 1));
            angles.push_back(pair<double, int> (0, 1));
            angles.push_back(pair<double, int> (end, -1));
        }
        else 
        {
            angles.push_back(pair<double, int> (start, 1));
            angles.push_back(pair<double, int> (end, -1));
        }
        

    }
    inline double dist(int x1, int y1, int x2, int y2)
    {
        
        double a = (x1 - x2) * (x1 - x2);
        
        double b = (y1 - y2) * (y1 - y2);

        return sqrt(a + b);
    }

    int maxWindows(vector<pair<double, int>>& angles)
    {
        sort(angles.begin(), angles.end(), myCmp());
        int res = 0;
        int sum = 0;
        for (int i = 0; i < angles.size(); i++)
        {
            //printf("%f (%f pi)\t%d\n", angles[i].first, angles[i].first / M_PI, angles[i].second);
            sum += angles[i].second;
            res = max(sum, res);
        }

        return res;

    }
    int fixedPoint(vector<vector<int>>& points, int k, int r)
    {
        int res = 0;
        vector<pair<double, int>> angles;

        for (int i = 0; i < points.size(); i++)
        {
            
            int x = points[i][0] - points[k][0];
            int y = points[i][1] - points[k][1];
            if (x == 0 && y == 0)
            {
                res += 1;
                continue;
            }
            getWindow(angles, x, y, r);
        }
        return res + maxWindows(angles);
    }

    int numPoints(vector<vector<int>>& points, int r) 
    {
        int ans = 0;
        for (int i = 0; i < points.size(); i++)
        {
            //printf("%d, %d\n", points[i][0], points[i][1]);
            int res = fixedPoint(points, i, r);
            //printf("fixed point (%d, %d), result = %d\n", points[i][0], points[i][1], res);
            ans = max(ans, res);

            //break;
        }

        return ans;
    }
};