// https://leetcode-cn.com/problems/remove-max-number-of-edges-to-keep-graph-fully-traversable/

// greedy
class Solution {
public:
    int fatherA[100005];
    int fatherB[100005];
    int findFather(int father[], int t)
    {
        if (father[t] == t)
        {
            return t;
        }
        father[t] = findFather(father, father[t]);
        return father[t];
    }
    int maxNumEdgesToRemove(int n, vector<vector<int>>& edges) 
    {
        int count = 0;
        for (int i = 1; i <= n; i++)
        {
            fatherA[i] = i;
            fatherB[i] = i;
        }
        for (int i = 0; i < edges.size(); i++)
        {
            if (edges[i][0] != 3)
            {
                continue;
            }
            int &x = edges[i][1];
            int &y = edges[i][2];
            if (findFather(fatherA, x) != findFather(fatherA, y))
            {
                count += 1;
                fatherA[findFather(fatherA, x)] = findFather(fatherA, y);
                fatherB[findFather(fatherB, x)] = findFather(fatherB, y);
            }
        }
        int countA = count;
        int countB = count;
        for (int i = 0; i < edges.size(); i++)
        {
            int &x = edges[i][1];
            int &y = edges[i][2];
            if (edges[i][0] == 1)
            {
                if (findFather(fatherA, x) != findFather(fatherA, y))
                {
                    countA += 1;
                    fatherA[findFather(fatherA, x)] = findFather(fatherA, y);
                }
            }
            else if (edges[i][0] = 2)
            {
                if (findFather(fatherB, x) != findFather(fatherB, y))
                {
                    countB += 1;
                    fatherB[findFather(fatherB, x)] = findFather(fatherB, y);
                }
            }
        }
        if (countA == n - 1 && countB == n - 1)
        {
            return (edges.size() - (countA + countB - count));
        }
        else 
        {
            return -1;
        }
        
    }
};