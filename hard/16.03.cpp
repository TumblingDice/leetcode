// https://leetcode-cn.com/problems/intersection-lcci/
class Solution {
public:


    class Vector
    {
    public:
        double x;
        double y;
    };

    class Node
    {
    public:
        double x;
        double y;
        Node() {}
        Node(double x, double y)
        {
            this->x = x;
            this->y = y;
        }
        bool operator < (const Node& other) const 
        {
            if ((this->x) == (other.x))
            {
                return ((this->y) < (other.y));
            }
            return ((this->x) < (other.x));
        }
        friend Node min(const Node& x, const Node& y)
        {
            return (x < y) ? x : y;
        }
        friend Node max(const Node& x, const Node& y)
        {
            return (x < y) ? y : x;
        }
        Node& operator = (const Node& other) 
        {
            this->x = other.x;
            this->y = other.y;
            return (*this);
        }
    };

    class Line
    {
    public:
        Node start;
        Node end;
        Vector direction;
        Line(int x1, int y1, int x2, int y2): start(x1, y1), end(x2, y2)
        {
            /*start.x = x1;
            start.y = y1;
            end.x = x2;
            end.y = y2;*/
            int dx = x2 - x1;
            int dy = y2 - y1;
            printf("dx = %d, dy = %d\n", dx, dy);
            direction.x = dx;
            direction.y = dy;
            /*
            if (dx == 0 && dy == 0)
            {
                direction.x = 0;
                direction.y = 0;
            }
            else if (dx == 0)
            {
                direction.x = 0;
                direction.y = 1;
            }
            else if (dy == 0)
            {
                direction.x = 1;
                direction.y = 0;
            }
            else 
            {
                int gcd = __gcd(dx, dy);
                direction.x = dx / gcd;
                direction.y = dy / gcd;
            }
            */
        }
    
        Node operator & (const Line& l2) const
        {
            vector<vector<double>> A(2);
            A[0].push_back(this->direction.x);
            A[0].push_back(l2.direction.x);
            A[1].push_back(this->direction.y);
            A[1].push_back(l2.direction.y);
            A[0].push_back(l2.start.x - this->start.x);
            A[1].push_back(l2.start.y - this->start.y);

            vector<double> solution(2);
            int res = gaussianElimination(A, 2, 3, solution);
            printf("res: %d\n", res);

            Node t;

            if (res == 0)
            {
                if (solution[0] <= 1 && solution[0] >= 0 && solution[1] <= 0 && solution[1] >= -1)
                {
                    t.x = this->start.x + solution[0] * this->direction.x;
                    t.y = this->start.y + solution[0] * this->direction.y;
                }
                else 
                {
                    t.x = NAN;
                    t.y = NAN;
                }
                
            
                
            }
            else if (res == -1)
            {
                t.x = NAN;
                t.y = NAN;
            }
            else
            {

                if ((max(this->start, this->end) < min(l2.start, l2.end)) || (max(l2.start, l2.end) < min(this->start, this->end)))
                {
                    t.x = NAN;
                    t.y = NAN;
                }
                else 
                {
                    printf("(%f, %f), (%f, %f)\n", this->start.x, this->start.y, this->end.x, this->end.y);
                    printf("%d\n", (this->start < this->end));
                    printf("min: (%f, %f)\t(%f, %f)\n", min(this->start, this->end).x, min(this->start, this->end).y, min(l2.start, l2.end).x, min(l2.start, l2.end).y);
                    t = max(min(this->start, this->end), min(l2.start, l2.end));
                }
                
                
            }
            printf("%f %f\n", t.x, t.y);
            return t;
        }
    /*
    dx1 dx2             t1          sx2-sx1
    dy1 dy2             t2          sy2-sy1
    */
    private:
        // 以前写过挺多并行的这个
        // -1:无解, 0:唯一解, 1:无穷多解
        static int gaussianElimination(vector<vector<double>>& A, int m, int n, vector<double>& solution)
        {
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    printf("%f ", A[i][j]);
                }
                printf("\n");
            }
            printf("\n");

            int rank = 0;
            int rankAug = 0;

            for (int i = 0; i < m; i++)
            {
                printf("at row#%d\n", i);
                int j = 0;
                while (j < n && A[i][j] == 0)
                {
                    j += 1;
                }
                printf("main: %d\n", j);
                if (j == n)
                {
                    continue;
                }
                rankAug += 1;
                if (j < (n - 1))
                {
                    rank += 1;
                }

                for (int k = n - 1; k >= j; k--)
                {
                    A[i][k] = A[i][k] / A[i][j];
                }
                for (int k = 0; k < m; k++)
                {
                    if (k == i)
                    {
                        continue;
                    }
                    double tmp = A[k][j];
                    for (int l = 0; l < n; l++)
                    {
                        printf("%d, %d = %f - (%f * %f)\n", k, l, A[k][l], tmp, A[i][l]);
                        A[k][l] = A[k][l] - (tmp * A[i][l]);
                    }
                }

                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        printf("%f ", A[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                printf("\n");
            }

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    printf("%f ", A[i][j]);
                }
                printf("\n");
            }

            if (rank != rankAug)
            {
                return -1;
            }
            if (rank == m)
            {
                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (A[i][j] != 0)
                        {
                            solution[j] = A[i][n - 1];
                            break;
                        }
                    }
                }
                return 0;
            }
            return 1;
        }
    
    };



    vector<double> intersection(vector<int>& start1, vector<int>& end1, vector<int>& start2, vector<int>& end2) 
    {
        Line l1(start1[0], start1[1], end1[0], end1[1]);
        Line l2(start2[0], start2[1], end2[0], end2[1]);

        vector<double> ans;
        Node res = l1 & l2;
        if (!isnan(res.x))
        {
            ans.push_back(res.x);
            ans.push_back(res.y);
        }
        return ans;
    }
};