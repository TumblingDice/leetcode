/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    /*
    class LoserTree
    {
    public:
        class Node
        {
        public:
            int loser;
            int l, r;
        };
        
        Node nodes[30005];
        int build(vector<ListNode*>& lists, int index)
        {
            if (nodes[index].l == nodes[index].r)
            {
                nodes[index].loser = -1;
                return (lists[nodes[index].r] == nullptr) ? -1 : nodes[index].r;
            }
            int mid = (nodes[index].l + nodes[index].r) / 2;
            nodes[index * 2].l = nodes[index].l;
            nodes[index * 2].r = mid;
            nodes[index * 2 + 1].l = mid + 1;
            nodes[index * 2 + 1].r = nodes[index].r;
            int leftWinner = build(lists, index * 2);
            int rightWinner = build(lists, index * 2 + 1);
            if (leftWinner == -1 && rightWinner == -1)
            {
                nodes[index].loser = -1;
                return -1;
            }
            else if (leftWinner == -1)
            {
                nodes[index].loser = -1;
                return rightWinner;
            }
            else if (rightWinner == -1)
            {
                nodes[index].loser = -1;
                return leftWinner;
            }
            nodes[index].loser = (lists[leftWinner]->val > lists[rightWinner]->val) ? leftWinner : rightWinner;
            return (lists[leftWinner]->val > lists[rightWinner]->val) ?  rightWinner : leftWinner;
        }

        // return winner
        int update(vector<ListNode*>& lists, int index, int t)
        {
            if (nodes[index].l == nodes[index].r)
            {
                lists[nodes[index].r] = lists[nodes[index].r]->next;
                return (lists[nodes[index].r] == nullptr) ? -1 : nodes[index].r;
            }
            int mid = (nodes[index].l + nodes[index].r) / 2;
            int winner = (t <= mid) ? (update(lists, index * 2, t)) : (update(lists, index * 2 + 1, t));
            if (nodes[index].loser == -1)
            {
                return winner;
            }
            else 
            {
                if (winner == -1)
                {
                    
                }
                nodes[index].loser = (nodes[index].loser > winner) ? (nodes[index].loser) : (winner);
                return 
            }
        }
    }
    /*
        3
     2     4
    1 2   3 4
    */
   struct myCmp
   {
       bool operator () (ListNode *a, ListNode *b)
       {
           return (a->val > b->val);
       }
   };
    ListNode* mergeKLists(vector<ListNode*>& lists) 
    {

        // 淦
        // 按理说这种外部排序, 败者树是常数最低的.
        // 但怎么处理有归并段读完的情况呢? 那样就不得不访问下一层节点, 
        // 看起来和heap比就没什么优越性了. 
        // 但! 如果只用于归并排序, 那么败者是不可能变空的.
        // 所以每一个node的更新, 实际上还是只需要更新的winner和上一轮的loser比就可以了. 
        // 实际上写起来特别麻烦. 


        // 算了用heap吧
        priority_queue<ListNode*, vector<ListNode*>, myCmp> pq;
        ListNode *head = new ListNode();
        ListNode *curNode = head;
        for (auto node: lists)
        {
            if (node == nullptr)
            {
                continue;
            }
            pq.push(node);    
        }
        while (!pq.empty())
        {
            ListNode *top = pq.top();
            curNode->next = new ListNode (top->val);
            curNode = curNode->next;
            pq.pop();
            if (top->next != nullptr)
            {
                pq.push(top->next);
            }
        }

        return head->next;
    }
};