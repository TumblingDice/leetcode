/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) 
    {
        if (k == 1)
        {
            return head;
        }
        ListNode *tmp = new ListNode();
        tmp->next = head;
        ListNode *curNode = tmp;
        ListNode *newHead;

        while (true)
        {
            ListNode *probe = curNode;
            for (int i = 0; i < k; i++)
            {

                
                if (probe->next == nullptr)
                {
                    return tmp->next;
                }
                probe = probe->next;
                //printf("probe: %d\n", probe->val);
            }
            ListNode *a = curNode->next;
            
            ListNode *b = a->next;
            ListNode *c;
            //printf("a: %d, b: %d\n", a->val, b->val);
            while (a != probe)
            {
                c = b->next;
                b->next = a;
                a = b;
                b = c;
            }
            //printf("%d %d\n", a->val, a->next->val);
            a = curNode->next;
            curNode->next = probe;
            curNode = a;
            a->next = c;

        }


        return tmp->next;
    }
};