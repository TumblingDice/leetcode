// https://leetcode-cn.com/problems/substring-with-concatenation-of-all-words/
class Solution {
public:
    multiset<int> patternSet;
    map<string, int> dict;
    int wordLength;
    int n;
    unsigned int windowLength = 0;
    inline unsigned long long hashValueOf(char x)
    {
        return (((unsigned long long ) (x)) - ((unsigned long long) ('a')))
                * (((unsigned long long ) (x)) - ((unsigned long long) ('a')));
    }
    bool check(string& s, int l)
    {
        multiset<int> windowSet;
        int r = windowLength + l;
        for (int i = l; i < r; i += wordLength)
        {
            string word = s.substr(i, wordLength);
            if (dict.find(word) == dict.end())
            {
                return false;
            }
            int index = dict[word];
            
            windowSet.insert(index);
            
        }
        return (windowSet == patternSet);
    }
    vector<int> findSubstring(string s, vector<string>& words) 
    {
        n = s.length();
        wordLength = words[0].length();
        vector<int> ans;
        
        
        unsigned long long patternHashSum = 0;
        unsigned long long windowHashSum = 0;
        

        int count = 0;
        for (string str: words)
        {
            if (dict.find(str) == dict.end())
            {
                dict[str] = count;
                count++;
            }
            int index = dict[str];
            windowLength += str.length();
            patternSet.insert(index);
            for (int i = 0; i < str.length(); i++)
            {
                patternHashSum += hashValueOf(str.at(i));
            }
        }
        //printf("%d %d\n", n, windowLength);
        if (n < windowLength)
        {
            return ans;
        }
        for (int i = 0; i < windowLength; i++)
        {
            windowHashSum += hashValueOf(s.at(i));
        }
        if (windowHashSum == patternHashSum)
        {
            if (check(s, 0))
            {
                ans.push_back(0);
            }
        }


        for (int i = 0; windowLength + i < n; i++)
        {
            windowHashSum += (hashValueOf(s.at(windowLength + i)) - hashValueOf(s.at(i)));
            if (windowHashSum != patternHashSum)
            {
                continue;
            }
            if (check(s, i + 1))
            {
                ans.push_back(i + 1);
            }
        }

        return ans;
    }
};