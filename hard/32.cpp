// https://leetcode-cn.com/problems/longest-valid-parentheses/


// ")()())"
class Solution 
{
public:
    int longestValidParentheses(string s) 
    {
        stack<int> parenStack;
        int n = s.length();
        int ans = 0;
        int offset = -1;
        for (int i = 0; i < n; i++)
        {
            if (s.at(i) == '(')
            {
                parenStack.push(i);
            }
            else if (s.at(i) == ')')
            {
                if (parenStack.size() != 0)
                {
                    parenStack.pop();
                    if (parenStack.size() == 0)
                    {
                        ans = max(ans, (i - offset));
                    }
                    else 
                    {
                        ans = max(ans, (i - parenStack.top()));
                    }
                }
                else 
                {
                    offset = i;
                }
            }
        }


        return ans;
    }
};