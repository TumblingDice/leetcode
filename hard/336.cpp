// https://leetcode-cn.com/problems/palindrome-pairs/

class Solution {
public:
    vector<vector<int> *> prefixPalin, postfixPalin;
    vector<string> *w;
    class Trie
    {
    public:
        
        struct Node
        {
            map<char, Node*> children;
            vector<int> cand;
            int end;
            Node()
            {
                end = -1;
            }
        };

        Node *root;
        Trie()
        {
            root = new Node();
        }
        void insertReverse(vector<string> *w, int idx)
        {
            Node *t = root;
            string& s = (*w)[idx];
            for (int i = s.length() - 1; i >= 0; i--)
            {
                if (t->children.find(s.at(i)) == t->children.end())
                {
                    t->children[s.at(i)] = new Node();
                }
                t = t->children[s.at(i)];
                t->cand.push_back(idx);
            }
            t->end = idx;
        }


        void print(Node *t)
        {/*
            for (int i: t->cand)
            {
                printf("%s\n", (*w)[i].c_str());
            }
            for (auto it = t->children.begin(); it != t->children.end(); it++)
            {
                char c = it->first;
                Node *next = it->second;
                printf("%c ->\n", c);
                print(next);
            }*/
        }
    };
    void manacher(vector<int>& pre, vector<int>& post, string& s)
    {
        int len = s.length();
        if (len == 0)
        {
            return;
        }
        string ss = "!";
        ss += s.at(0);
        
        
        for (int i = 1; i < len; i++)
        {
            ss += "#";
            ss += s.at(i);
        }
        ss += "$";
        //printf("%s\n", ss.c_str());
        int pos = 0;
        int mx = -1;
        vector<int> p(len * 2);
        for (int i = 1; i < len * 2; i++)
        {
            p[i] = (i <= mx) ? min(p[pos * 2 - i], mx - i) : 0;
            while (ss.at(i + p[i] + 1) == ss.at(i - p[i] - 1))
            {
                p[i]++;
            }
            if (mx < i + p[i])
            {
                mx = i + p[i];
                pos = i;
            }

            if (i - p[i] == 1)
            {
                pre[(i + p[i]) / 2] = 1;
            }
            if (i + p[i] == len * 2 - 1)
            {
                post[(i - p[i]) / 2] = 1;
            }
        }
        
	}

    bool isPrefixPalin(int idx, int pos)
    {
        if (pos == 0)
        {
            return true;
        }
        return (*(prefixPalin[idx]))[pos - 1] == 1;
    }
    bool isPostfixPalin(int idx, int pos)
    {
        return (*(postfixPalin[idx]))[pos] == 1;
    }
    vector<vector<int>> palindromePairs(vector<string>& words) 
    {
        w = &words;
        Trie trie;
        
        
        for (int i = 0; i < words.size(); i++)
        {
            trie.insertReverse(&words, i);
            prefixPalin.push_back(new vector<int> (words[i].length()));
            postfixPalin.push_back(new vector<int> (words[i].length()));
            manacher(*(prefixPalin.back()), *(postfixPalin.back()), words[i]);
            trie.root->cand.push_back(i);
        }
        //abaaba
        //trie.print(trie.root);
        vector<vector<int>> ans;
        
        for (int i = 0; i < words.size(); i++)
        {
            
            Trie::Node *t = trie.root;
            int j = 0;
            while (true)
            {
                if (j == words[i].length())
                {
                    for (int idx: t->cand)
                    {
                        if (idx == i)
                        {
                            continue;
                        }
                        if (isPrefixPalin(idx, (words[idx].length() - j)))
                        {
                            vector<int> tmp;
                            tmp.push_back(i);
                            tmp.push_back(idx);
                            ans.push_back(tmp);
                        }
                    }
                    break;
                }
                if (t->end != -1)
                {
                    if (isPostfixPalin(i, j))
                    {
                        vector<int> tmp;
                        tmp.push_back(i);
                        tmp.push_back(t->end);
                        ans.push_back(tmp);
                    }
                }
                if (t->children.find(words[i].at(j)) == t->children.end())
                {
                    break;
                }
                t = t->children[words[i].at(j)];
                j += 1;
            }
        }
        
        

        return ans;
    }
};