// https://leetcode-cn.com/problems/first-missing-positive/


class Solution {
public:
    int firstMissingPositive(vector<int>& nums) 
    {
        for (int i = 0; i < nums.size(); i++)
        {
            while (true)
            {
                //printf("i = %d, nums[i] = %d\n", i, nums[i]);
                // 突然想到, 一直默认的c if语句的所谓的"短路规则", 是语言标准吗? 还是依赖实现.
                if ((nums[i] <= 0)  || (nums[i] >= nums.size()) || (nums[i] == nums[nums[i] - 1]))
                {
                    break;
                }
                swap(nums[i], nums[nums[i] - 1]);
            }
        }


        for (int i = 0; i < nums.size(); i++)
        {
            if (nums[i] != (i + 1))
            {
                return i + 1;
            }
        }

        return nums.size() + 1;
    }
};