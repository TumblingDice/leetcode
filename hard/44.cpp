// https://leetcode-cn.com/problems/wildcard-matching/
class Solution {
public:
    string target;
    string pattern;
    map<pair<int, int>, bool> dp;
    bool solve(pair<int, int> p)
    {
        int i = p.first;
        int j = p.second;
        //printf("%d %d\n", i, j);
        if (j == 0 && i != 0)
        {
            return false;
        }
        if (dp.find(p) != dp.end())
        {
            return dp[p];
        }
        //printf("???\n");
        pair<int, int> next;
        if (pattern[j - 1] == '?')
        {
            if (i == 0)
            {
                dp[p] = false;
                return false;
            }
            next.first = i - 1;
            next.second = j - 1;
            
            bool ret = solve(next);
            dp[p] = ret;
            return ret;
        }
        else if (pattern[j - 1] == '*')
        {
            bool res1, res2;
            next.first = i - 1;
            next.second = j;
            res1 = solve(next);
            next.first = i;
            next.second = j - 1;
            res2 = solve(next);

            dp[p] = res1 || res2;
            return res1 || res2;
        }
        else 
        { 
            if (i == 0)
            {
                dp[p] = false;
                return false;
            }
            if (pattern[j - 1] == target[i - 1])
            {
                next.first = i - 1;
                next.second = j - 1;
                bool ret = solve(next);
                dp[p] = ret;
                return ret;
            }
            else 
            {
                dp[p] = false;
                return false;
            }
        }
    }
    bool isMatch(string s, string p) 
    {
        pattern = p;
        target = s;
        pair<int, int> tmp;
        tmp.first = 0;
        tmp.second = 0;
        dp[tmp] = true;

        tmp.first = s.length();
        tmp.second = p.length();

        return solve(tmp);
    }
};