// https://leetcode-cn.com/problems/jump-game-ii/

class Solution {
public:

    struct Stone 
    {
        int pos;
        int val;
        
        bool operator < (const Stone& y) const 
        {
            return ((this->pos + this->val) < (y.pos + y.val));
        }

        Stone(int p, int v)
        {
            this->pos = p;
            this->val = v;
        }
    };
    int jump(vector<int>& nums) 
    {
        //priority_queue<int, stack<int>, less<int>> pq;
        priority_queue<Stone, vector<Stone>, less<Stone>> pq;
        int ans = 0;
        int n = nums.size();
        if (n == 1)
        {
            return 0;
        }


        pq.push(Stone(0, nums[0]));

        while (true)
        {
            
            Stone t = pq.top();
            pq.pop();
            //printf("top: nums[%d] = %d\n", t.pos, t.val);
            ans += 1;
            if (t.pos + t.val >= (n - 1))
            {
                break;
            }
            
            for (int i = t.pos + 1; i <= min(t.pos + t.val, n - 1); i++)
            {
                pq.push(Stone(i, nums[i]));
            }
            
        }
        return ans;




    }
};