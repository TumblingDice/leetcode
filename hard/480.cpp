// https://leetcode-cn.com/problems/sliding-window-median/

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
class Solution {
public:
    // 这东西起码能自定义个update operator, 然后有
    // 能access children的iterator. 

    // 题解里用pd_ ds的做法是把相同的num加个id当做key, 不用value.
    // 但应该把相同num的个数当做value, 这样search会快很多.

    template<class Node_CItr, class Node_Itr, class Cmp_Fn, class _Alloc>
    struct my_node_update
    {

        typedef int metadata_type;
        int get_kth(int k)
        {
            
            // 据我观察这个node_begin()应该是root, 很合理
            Node_CItr it = node_begin();


            while (it != node_end())
            {
                
                Node_CItr lChild = it.get_l_child();
                Node_CItr rChild = it.get_r_child();

                
                int leftSum = (lChild == node_end()) ? 0 : lChild.get_metadata();
                int rightSum = (rChild == node_end()) ? 0 : rChild.get_metadata();
                //printf("???\n");
                int curSum = (*it)->second;



                /*printf("cur: (%d, %d), l: (%d, %d), r: (%d, %d)\n", 
                        (*it)->first, (*it)->second,
                        (*lChild)->first, (*lChild)->second,
                        (*rChild)->first, (*rChild)->second);*/
                //printf("leftSum: %d, curSum: %d, rightSum: %d\n", leftSum, curSum, rightSum);
                if (leftSum >= k)
                {
                    it = lChild;
                }
                else if (k <= (leftSum + curSum)) 
                {
                    return (*it)->first;
                }
                else 
                {
                    k -= (curSum + leftSum);
                    it = rChild;
                }
            }

            return -1;
        }

        inline void operator() (Node_Itr it, Node_CItr end_it)
        {
            Node_Itr lChild = it.get_l_child();
            Node_Itr rChild = it.get_r_child();
            
            int leftSum = (lChild == end_it) ? 0 : lChild.get_metadata();
            int rightSum = (rChild == end_it) ? 0 : rChild.get_metadata();
            
            const_cast<metadata_type &>(it.get_metadata()) = leftSum + rightSum + (*it)->second;
        }

        virtual Node_CItr node_begin() const = 0;
        virtual Node_CItr node_end() const = 0;
    };

    
    __gnu_pbds::tree<int, 
                    int, 
                    less<int>, 
                    __gnu_pbds::rb_tree_tag, 
                    my_node_update>
            rbTree;
    void increase(int t)
    {
        auto res = rbTree.find(t);
        if (res != rbTree.end())
        {
            // 这里要先删了再插, 否则不会触发metadata update
            int tmp = res->second;
            rbTree.erase(res);
            rbTree.insert(make_pair(t, tmp + 1));
        }
        else 
        {
            rbTree.insert(make_pair(t, 1));
        }
    }

    void decrease(int t)
    {
        auto res = rbTree.find(t);
        // assertion: res not null
        int tmp = res->second;
        rbTree.erase(res);
        if (tmp > 1)
        {
            rbTree.insert(make_pair(t, tmp - 1));
        }
    }
    double median(int k)
    {
        if (k % 2 == 0)
        {
            double a = (double) rbTree.get_kth(k / 2);
            double b = (double) rbTree.get_kth(k / 2 + 1);
            return (a + b) / 2;
        }
        else 
        {
            double a = (double) rbTree.get_kth(k / 2 + 1);
            return a;
        }
        
    }
    vector<double> medianSlidingWindow(vector<int>& nums, int k) 
    {

        
        vector<double> ans;
        
        for (int i = 0; i < k; i++)
        {
            increase(nums[i]);
        }
        ans.push_back(median(k));

        for (int i = 0; k + i < nums.size(); i++)
        {
            decrease(nums[i]);
            increase(nums[k + i]);
            ans.push_back(median(k));
        }
        //printf("%d\n", rbTree.get_kth(2));
        return ans;
    }
};