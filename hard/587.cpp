// https://leetcode-cn.com/problems/erect-the-fence/

class Solution {
public:


    class Node
    {
    public:
        int x;
        int y;
        Node() {}
        Node(int x, int y)
        {
            this->x = x;
            this->y = y;
        }
        bool operator < (const Node& other) const 
        {
            if ((this->y) == (other.y))
            {
                return ((this->x) < (other.x));
            }
            return ((this->y) < (other.y));
        }
        friend Node min(const Node& x, const Node& y)
        {
            return (x < y) ? x : y;
        }
        friend Node max(const Node& x, const Node& y)
        {
            return (x < y) ? y : x;
        }
        Node& operator = (const Node& other) 
        {
            this->x = other.x;
            this->y = other.y;
            return (*this);
        }

        int leftOf(const Node& a, const Node& b) const 
        {
            
            int ret = ((a.x * b.y) - (a.y * b.x) + (b.x * this->y) - (b.y * this->x)
                    + (this->x * a.y) - (this->y * a.x));
                    
            //printf("(%d, %d) left of (%d, %d), (%d, %d) = %d\n", this->x, this->y, a.x, a.y, b.x, b.y, ret);
            return ret;
        }
        
    };
    
    class Vector
    {
    public:
        int x;
        int y;
        Vector(int x, int y)
        {
            this->x = x;
            this->y = y;
        }
        Vector() {}
        int operator ^ (const Vector& other) const
        {
            return (this->x * other.x) + (this->y * other.y);
        }
        // its mod in fact
        int operator * (const Vector& other) const
        {
            return (this->x * other.y - this->y * other.x);
        }
    };
    
    struct MyCmp
    {
        Node base;
        int dist(const Node& a, const Node& b)
        {
            return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
        }
        bool operator () (const Node& x, const Node& y)
        {
            int res = y.leftOf(base, x);
            if (res > 0)
            {
                return true;
            }
            else if (res == 0)
            {
                return (dist(base, x) < dist(base, y));
            }
            else 
            {
                return false;
            }
        }
    };

    vector<vector<int>> outerTrees(vector<vector<int>>& points) 
    {
        int n = points.size();


        if (n <= 3)
        {
            return points;
        }


        vector<Node> nodes;
        vector<vector<int>> ans;
        MyCmp myCmp;
        for (int i = 0; i < n; i++)
        {
            nodes.push_back(Node(points[i][0], points[i][1]));
        }
        
        for (int i = 0; i < n - 1; i++)
        {
            if (nodes[i] < nodes[n - 1])
            {
                swap(nodes[i], nodes[n - 1]);
            }
        }
        myCmp.base = nodes[n - 1];
        nodes.pop_back();
        printf("base: (%d, %d)\n", myCmp.base.x, myCmp.base.y);

        /*
        vector<int> tmp(2);
        tmp[0] = myCmp.base.x;
        tmp[1] = myCmp.base.y;
        ans.push_back(tmp)
        */


        sort(nodes.begin(), nodes.end(), myCmp);
        

        int k = nodes.size() - 1;
        while (k >= 0 && (nodes[k].leftOf(myCmp.base, nodes.back()) == 0))
        {
            k--;
        }
        printf("k = %d, (%d, %d)\n", k, nodes[k + 1].x, nodes[k + 1].y);
        for (int i = k + 1, j = nodes.size() - 1; i < j; i++, j--)
        {
            printf("swap (%d, %d) and (%d, %d)\n", nodes[i].x, nodes[i].y, nodes[j].x, nodes[j].y);
            swap(nodes[i], nodes[j]);
        }
        for (auto t: nodes)
        {
           
           printf("%d %d\n", t.x, t.y);
        }
        vector<Node> stk;
        stk.push_back(myCmp.base);
        stk.push_back(nodes[0]);
        for (int i = 1; i < nodes.size(); i++)
        {
            printf("for node (%d, %d)\n", nodes[i].x, nodes[i].y);
            // 凸包和此题不一样的唯一一点就是, 此题要求"在边界上的所有点"
            while ((stk.size() > 1) && (nodes[i].leftOf(stk[stk.size() - 2], stk[stk.size() - 1]) < 0))
            {
                printf("popping (%d, %d)\n", stk.back().x, stk.back().y);
                stk.pop_back();
            }
            stk.push_back(nodes[i]);
        }
        vector<int> tmp(2);
        for (Node& t: stk)
        {
            
            tmp[0] = t.x;
            tmp[1] = t.y;
            ans.push_back(tmp);
        }
        
        return ans;
    }
};