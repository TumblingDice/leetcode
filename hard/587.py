import numpy
class Solution:
    def __init__(self):
        self.baseX = 0
        self.baseY = 0
    def outerTrees(self, points: List[List[int]]) -> List[List[int]]:
        self.baseX = points[0][0]
        self.baseY = points[0][1]
        
        for i in range(0, len(points)):
            if (self.baseY == points[i][1]):
                if (self.baseX > points[i][0]):
                    self.baseX = points[i][0]
                    self.baseY = points[i][1]
            elif (self.baseY > points[i][1]):
                self.baseX = points[i][0]
                self.baseY = points[i][1]
        
        