// https://leetcode-cn.com/problems/couples-holding-hands/solution/
class Solution {
public:
    int visit[555];
    int graph[77][88];
    int minSwapsCouples(vector<int>& row) 
    {
        memset(visit, 0, sizeof(visit));
        int ans = 0;

        for (int i = 0; i < row.size(); i += 2)
        {
            int a = row[i] / 2;
            int b = row[i + 1] / 2;
            if (a == b)
            {
                continue;
            }
            graph[a][b] = 1;
            graph[b][a] = 1;
        }
        // 如果有连通分量, 一定是个圈, 因为顶点的度要么是0, 要么是2.
        for (int i = 0; i < row.size() / 2; i++)
        {
            if (visit[i] == 1)
            {
                continue;
            }
            vector<int> q;
            int j = 0;
            q.push_back(i);
            visit[i] = 1;
            while (j < q.size())
            {
                int top = q[j];
                j += 1;
                
                for (int k = 0; k < row.size() / 2; k++)
                {
                    if (graph[top][k] == 1 && visit[k] == 0)
                    {
                        visit[k] = 1;
                        q.push_back(k);
                    }
                }
            }
            ans += (q.size() - 1);
        }


        return ans;
    }
};