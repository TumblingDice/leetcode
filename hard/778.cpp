// https://leetcode-cn.com/problems/swim-in-rising-water/
class Solution {
public:
    int father[5555];
    int findFather(int x)
    {
        if (father[x] == x)
        {
            return x;
        }
        father[x] = findFather(father[x]);
        return father[x];
    }
    void merge(int x, int y)
    {
        father[findFather(x)] = findFather(y);
    }
    inline int indexOf(int x, int y)
    {
        return x * n + y;
    }
    struct Grid
    {
        int x, y;
        int height;
        bool operator < (const Grid& y) const 
        {
            return this->height > y.height;
        }
    };
    int m, n;
    int swimInWater(vector<vector<int>>& grid) 
    {

        n = grid.size();
        if (n == 1)
        {
            return 0;
        }
        priority_queue<Grid> pq;
        for (int i = 0; i < n * n; i++)
        {
            father[i] = i;
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Grid tmp;
                tmp.x = i;
                tmp.y = j;
                tmp.height = grid[i][j];
                pq.push(tmp);
            }
        }
        Grid t;
        while (true)
        {
            if (findFather(indexOf(0, 0)) == findFather(indexOf(n - 1, n - 1)))
            {
                return max(t.height, grid[n - 1][n - 1]);
            }
            t = pq.top();
            pq.pop();
            
            int &x = t.x;
            int &y = t.y;
            //printf("%d %d\n", x, y);
            if (x > 0)
            {
                if (grid[x - 1][y] < grid[x][y])
                {
                    merge(indexOf(x - 1, y), indexOf(x, y));
                }
                
            }
            if (x < n - 1)
            {
                if (grid[x + 1][y] < grid[x][y])
                    merge(indexOf(x + 1, y), indexOf(x, y));
            }
            if (y > 0)
            {
                if (grid[x][y - 1] < grid[x][y])
                    merge(indexOf(x, y - 1), indexOf(x, y));
            }
            if (y < n - 1)
            {
                if (grid[x][y + 1] < grid[x][y])
                    merge(indexOf(x, y + 1), indexOf(x, y));
            }
        }
    }
};