// https://leetcode-cn.com/problems/maximum-frequency-stack/

class FreqStack 
{
public:
    struct myCmp
    {
        bool operator () (vector<int>* a, vector<int>* b) const
        {
            if (a->size() == b->size())
            {
                return (a->back() > b->back());
            }
            return a->size() > b->size();
        }
    };
    map<int, vector<int>*> dic1;
    map<vector<int>*, int, myCmp> dic2;
    int count;

    FreqStack() {
        count = 0;
    }
    

    void printDic()
    {
        /*printf("dic1:\n");
        for (auto p: dic1)
        {
            printf("%d\t{", p.first);
            for (auto i: *(p.second))
            {
                printf("%d ", i);
            }
            printf("}\n");
        }
        printf("dic2:\n");
        for (auto p: dic2)
        {
            printf("{");
            for (auto i: *(p.first))
            {
                printf("%d ", i);
            }
            printf("}\t%d\n", p.second);
        }*/
    }
    void push(int x) 
    {

        //printf("start pushing %d\n", x);

       //printDic();
        
        
        auto res = dic1.find(x);
        if (res == dic1.end())
        {
            
           // printf("not exist\n");
            vector<int>* s = new vector<int> ();
            s->push_back(count);
            dic1.insert(pair<int, vector<int>*> (x, s));
            dic2[s] = x;
        }
        else 
        {
           // printf("already in\n");
            vector<int>* s = res->second;
            dic2.erase(s);
            s->push_back(count);
            
            dic2.insert(pair<vector<int>*, int> (s, x));
        }
       // printDic();
        count += 1;
    }
    
    int pop() 
    {

       // printf("popping\n");
      //  printDic();
        auto res = dic2.begin();
        vector<int> *s = res->first;
        int x = res->second;
        dic2.erase(res);
        s->pop_back();
        if (s->size() == 0)
        {
           // printf("stack empty\n");
            dic1.erase(x);
        }
        else 
        {
            dic2.insert(pair<vector<int>*, int> (s, x));
        }
       // printDic();
        //printf("popping %d\n", x);
        return x;
    }
};
/**
 * Your FreqStack object will be instantiated and called as such:
 * FreqStack* obj = new FreqStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 */